require 'lfs' --luarocks install luafilesystem
require 'lsqlite3'
require 'csvigo'

function result_toNum(s)
	if(s == 'D') then return '0'
	elseif(s == 'A') then return '1'
	else return '-1' end
end

-- k=0 for j1_odds, k=1 for draw_odds and k=2 for j2_odds
function getOddsMean(matches, index, k, p)

	p = p or false
	sum = 0
	nb = 0

	for i=31, 69 do
		tmp = matches[index[i]]
		if(i%3 == k and tmp ~= nil and tmp ~= '') then
			return tmp
		end
	end
	return 1
end

-- Creation de la base
local db = sqlite3.open("../foot_database")

assert(db:exec[[CREATE TABLE IF NOT EXISTS matches(level, date, j1_id,
	j1_ftg, j1_htg, j2_id, j2_ftg, j2_htg, winner, winner_ht,
	attendance, referee, j1_s, j2_s, j1_st, j2_st, j1_hw, j2_hw, j1_c,
	j2_c, j1_f, j2_f, j1_o, j2_o, j1_y, j2_y, j1_r, j2_r, j1_bp, j2_bp,
	j1_odds, draw_odds, j2_odds, PRIMARY
	KEY(date, j1_id, j2_id))
	]])

assert(db:execute ("BEGIN TRANSACTION;"))

for file in lfs.dir[[../Sources/]] do
	if string.sub(file, -3) == "csv" then
		-- Ouverture des fichier matches
		local matches = csvigo.load({path = "../Sources/" .. file, mode="large"})
		print("A partir du fichier : Sources/" .. file)
		-- Creation de la table matches
		local index = {}
		for j=1, #matches[1] do
			if(matches[1][j] == 'Div') then index[1] = j
			elseif(matches[1][j] == 'Date') then index[2] = j
			elseif(matches[1][j] == 'HomeTeam') then index[3] = j
			elseif(matches[1][j] == 'FTHG') then index[4] = j
			elseif(matches[1][j] == 'HTHG') then index[5] = j
			elseif(matches[1][j] == 'AwayTeam') then index[6] = j
			elseif(matches[1][j] == 'FTAG') then index[7] = j
			elseif(matches[1][j] == 'HTAG') then index[8] = j
			elseif(matches[1][j] == 'FTR') then index[9] = j
			elseif(matches[1][j] == 'HTR') then index[10] = j
			elseif(matches[1][j] == 'Attendance') then index[11] = j
			elseif(matches[1][j] == 'Referee') then index[12] = j
			elseif(matches[1][j] == 'HS') then index[13] = j
			elseif(matches[1][j] == 'AS') then index[14] = j
			elseif(matches[1][j] == 'HST') then index[15] = j
			elseif(matches[1][j] == 'AST') then index[16] = j
			elseif(matches[1][j] == 'HHW') then index[17] = j
			elseif(matches[1][j] == 'AHW') then index[18] = j
			elseif(matches[1][j] == 'HC') then index[19] = j
			elseif(matches[1][j] == 'AC') then index[20] = j
			elseif(matches[1][j] == 'HF') then index[21] = j
			elseif(matches[1][j] == 'AF') then index[22] = j
			elseif(matches[1][j] == 'HO') then index[23] = j
			elseif(matches[1][j] == 'AO') then index[24] = j
			elseif(matches[1][j] == 'HY') then index[25] = j
			elseif(matches[1][j] == 'AY') then index[26] = j
			elseif(matches[1][j] == 'HR') then index[27] = j
			elseif(matches[1][j] == 'AR') then index[28] = j
			elseif(matches[1][j] == 'HBP') then index[29] = j
			elseif(matches[1][j] == 'ABP') then index[30] = j
			elseif(matches[1][j] == 'GBH') then index[31] = j
			elseif(matches[1][j] == 'GBD') then index[32] = j
			elseif(matches[1][j] == 'GBA') then index[33] = j
			elseif(matches[1][j] == 'IWH') then index[34] = j
			elseif(matches[1][j] == 'IWD') then index[35] = j
			elseif(matches[1][j] == 'IWA') then index[36] = j
			elseif(matches[1][j] == 'LBH') then index[37] = j
			elseif(matches[1][j] == 'LBD') then index[38] = j
			elseif(matches[1][j] == 'LBA') then index[39] = j
			elseif(matches[1][j] == 'SBH') then index[40] = j
			elseif(matches[1][j] == 'SBD') then index[41] = j
			elseif(matches[1][j] == 'SBA') then index[42] = j
			elseif(matches[1][j] == 'WHH') then index[43] = j
			elseif(matches[1][j] == 'WHD') then index[44] = j
			elseif(matches[1][j] == 'WHA') then index[45] = j
			elseif(matches[1][j] == 'PSH') then index[46] = j
			elseif(matches[1][j] == 'PSD') then index[47] = j
			elseif(matches[1][j] == 'PSA') then index[48] = j
			elseif(matches[1][j] == 'SOH') then index[49] = j
			elseif(matches[1][j] == 'SOD') then index[50] = j
			elseif(matches[1][j] == 'SOA') then index[51] = j
			elseif(matches[1][j] == 'SJH') then index[52] = j
			elseif(matches[1][j] == 'SJD') then index[53] = j
			elseif(matches[1][j] == 'SJA') then index[54] = j
			elseif(matches[1][j] == 'SYH') then index[55] = j
			elseif(matches[1][j] == 'SYD') then index[56] = j
			elseif(matches[1][j] == 'SYA') then index[57] = j
			elseif(matches[1][j] == 'VCH') then index[58] = j
			elseif(matches[1][j] == 'VCD') then index[59] = j
			elseif(matches[1][j] == 'VCA') then index[60] = j
			elseif(matches[1][j] == 'B365H') then index[61] = j
			elseif(matches[1][j] == 'B365D') then index[62] = j
			elseif(matches[1][j] == 'B365A') then index[63] = j
			elseif(matches[1][j] == 'BSH') then index[64] = j
			elseif(matches[1][j] == 'BSD') then index[65] = j
			elseif(matches[1][j] == 'BSA') then index[66] = j
			elseif(matches[1][j] == 'BWH') then index[67] = j
			elseif(matches[1][j] == 'BWD') then index[68] = j
			elseif(matches[1][j] == 'BWA') then index[69] = j
			end
		end
		
		for i=2, #matches do
			sql = string.format([[
				INSERT INTO matches VALUES 
					("%s", "%s", "%s", "%s", "%s", "%s", "%s", 
					"%s", "%s", "%s", "%s", "%s", "%s", "%s", 
					"%s", "%s", "%s", "%s", "%s", "%s", "%s", 
					"%s", "%s", "%s", "%s", "%s", "%s", "%s", 
					"%s", "%s", "%s", "%s", "%s");
				]], 
				matches[i][index[1]],
				matches[i][index[2]],
				matches[i][index[3]],
				matches[i][index[4]],
				matches[i][index[5]],
				matches[i][index[6]],
				matches[i][index[7]],
				matches[i][index[8]],
				result_toNum(matches[i][index[9]]),
				result_toNum(matches[i][index[10]]),
				matches[i][index[11]],
				matches[i][index[12]],
				matches[i][index[13]],
				matches[i][index[14]],
				matches[i][index[15]],
				matches[i][index[16]],
				matches[i][index[17]],
				matches[i][index[18]],
				matches[i][index[19]],
				matches[i][index[20]],
				matches[i][index[21]],
				matches[i][index[22]],
				matches[i][index[23]],
				matches[i][index[24]],
				matches[i][index[25]],
				matches[i][index[26]],
				matches[i][index[27]],
				matches[i][index[28]],
				matches[i][index[29]],
				matches[i][index[30]],
				getOddsMean(matches[i], index, 1),
				getOddsMean(matches[i], index, 2),
				getOddsMean(matches[i], index, 0)
			)
			assert (db:execute(sql))
		end
	end
end

assert(db:execute ("COMMIT;"))
