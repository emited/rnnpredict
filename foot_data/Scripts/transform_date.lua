require 'lfs' --luarocks install luafilesystem
require 'lsqlite3'
require 'csvigo'

-- Ouverture de la base
local db = sqlite3.open("../foot_database")
assert(db:execute ("BEGIN TRANSACTION;"))
for line in db:nrows('SELECT distinct(p.date) FROM matches p') do
	date = line["date"]
	new = {}
	for w in date:gmatch("[^/]*") do
		table.insert(new, w)
	end
	if(new[1] and new[3] and new[5]) then
		day = new[1]
		month = new[3]
		year = new[5]
		new = '20'..year..'-'..month..'-'..day
		sql = string.format([[
			UPDATE matches
			SET date = "%s"
			WHERE date = "%s";
			]], new, date)
		assert(db:execute(sql))
	end
end
assert(db:execute ("COMMIT;"))


assert(db:execute ("BEGIN TRANSACTION;"))
for line in db:nrows('SELECT distinct(p.date) FROM matches p') do
	date = line["date"]
	if(date:find("2020%d")) then
		new = date:sub(3,-1)
		sql = string.format([[
			UPDATE matches
			SET date = "%s"
			WHERE date = "%s";
			]], new, date)
		assert(db:execute(sql))
	end
end
assert(db:execute ("COMMIT;"))
