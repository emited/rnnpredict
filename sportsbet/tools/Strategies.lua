local Strategy = torch.class('sportsbet.Strategies')

function getOdds(match)
	return {
		[-1] = match['j1_odds'],
		[0] = match['draw_odds'] or 0,
		[1] = match['j2_odds']
	}
end

function Strategy:__init()

	if(self.params == nil) then
		self.params = {} 
	end
	if(self.stratLabels == nil) then
		self.stratLabels = {}
	end
	if(self.data == nil) then
		self.data = {}
	end
end

-- Places a bet of 1 for every match predicted.
local NaiveStrat = torch.class('sportsbet.NaiveStrat', 'sportsbet.Strategies')

function NaiveStrat:__init(data, labels, amount)

	self.params = {strat="naive", amount=amount}
	self.data = data
	self.stratLabels = {}	

	if(#data ~= #labels) then
		error('Data is size '..#data..', and stratLabels is size '..#labels..'.\n')
	end
	for i = 1, #data do
		local match = data[i]
		if(labels[i] ~= 'nil' and match['j1_odds'] and match['j2_odds']) then
			table.insert(self.stratLabels, {
				winner = labels[i],
				amount = amount,
				match = match
				}
			)
		else table.insert(self.stratLabels, 'nil')
		end
	end
	sportsbet.Strategies(self)
end


-- places a bet of amount for every match predicted, only if estimated[prob] > implied[prob].
local ProbStrat = torch.class('sportsbet.ProbStrat', 'sportsbet.Strategies')

function ProbStrat:__init(data, labels, amount, probs)

	self.params = {strat="prob", amount=amount}
	self.data = data
	self.stratLabels = {}

	if #data ~= #labels then
		error('Data is size '..#data..', and stratLabels is size '..#labels..'.\n')
	end
	for i = 1, #data do
		local match = data[i]
		if(labels[i] ~= 'nil' and match['j1_odds'] and match['j2_odds']) then
			local odds = getOdds(match)
			local impliedProb = odds[labels[i]]/(odds[-1] + odds[1] + odds[0])
			if(probs[i] > impliedProb) then
				table.insert(self.stratLabels, {
					winner = labels[i],
					amount = amount,
					match = match,
					estimProb = probs[i]
					}
				)
			else table.insert(self.stratLabels, 'nil')
			end	
		else table.insert(self.stratLabels, 'nil')
		end
	end
	sportsbet.Strategies(self)
end


-- places bets of amount given by the kelly criterion, only if estimated[prob] > implied[prob].
local KellyStrat = torch.class('sportsbet.KellyStrat', 'sportsbet.Strategies')

function KellyStrat:__init(data, labels, amount, probs)

	self.params = {strat="kelly", amount=amount}
	self.data = data
	self.stratLabels = {}

	if #data ~= #labels then
		error('Data is size '..#data..', and stratLabels is size '..#labels..'.\n')
	end
	for i = 1, #data do
		local match = data[i]
		if(labels[i] ~= 'nil' and match['j1_odds'] and match['j2_odds']) then
			local odds = getOdds(match)
			local impliedProb = odds[labels[i]]/(odds[-1] + odds[1] + odds[0])
			if(probs[i] > impliedProb) then
				local amount = (probs[i]*odds[labels[i]]-1)/(odds[labels[i]]-1)
				if(amount > 0) then
					table.insert(self.stratLabels, {
						winner = labels[i],
						amount = amount,
						match = match,
						estimProb = probs[i]
						}
					)
				else table.insert(self.stratLabels, 'nil')
				end
			else table.insert(self.stratLabels, 'nil')
			end	
		else table.insert(self.stratLabels, 'nil')
		end
	end
	sportsbet.Strategies(self)
end

function Strategy:evaluate()

	if(#self.data ~= #self.stratLabels) then
		error('Data is size '..#self.data..', and stratLabels is size '..#self.stratLabels..'.\n')
	end
	
	self.gain = {0}
	local totalGain = 0
	self.cost = {0}
	local totalCost = 0
	local betNumber = 0

	for i = 1, #self.data do
		local matchStrat = self.stratLabels[i]

		if matchStrat ~= 'nil' then
			local match = self.data[i]
			local odds = getOdds(match)
			betNumber = betNumber + 1
			stake = matchStrat['amount']
			if(matchStrat['winner'] == match['winner']) then
				table.insert(self.gain, stake*odds[matchStrat['winner']])
				totalGain = totalGain + stake*odds[matchStrat['winner']]
			else
				table.insert(self.gain, 0)
			end
			table.insert(self.cost, stake)
			totalCost = totalCost + stake
		else
			table.insert(self.gain, 0)
			table.insert(self.cost, 0)
		end
	end
	if(totalCost == 0) then return 0, 0 end
	return (totalGain - totalCost)/totalCost, totalGain - totalCost, betNumber
end
