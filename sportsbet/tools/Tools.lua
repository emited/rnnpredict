require 'lsqlite3'
require 'lfs'

plus = debug.getinfo(1).source:match("@(.*/)")
if(plus == nil) then plus = '' end
currentPath  =  lfs.currentdir()
               
local Tools = torch.class('sportsbet.Tools');



function Tools:loadDB(path, Tmin, Tmax, params)

	print('Loading Database...')
	print(params)

	params = params or {"j1_id", "j2_id", "winner", "date", "j1_odds", "j2_odds"}
	args = ''
	for j in self:list_iter(params) do 
		args = args .. 'p.' .. j .. ', '
	end
	if args ~= '' then
		args = args:sub(1, -3)
	end
	local db  =  sqlite3.open(path)
	local data  =  {}
	local sql  =  string.format(
		[[SELECT %s 
		FROM MATCHES p WHERE p.date > "%s" and p.date < "%s" order by p.date]],
		args, Tmin, Tmax)
	local i  =  1
	for row in db:nrows(sql) do
		data[i]  =  row
		
		-- data transformation
		data[i]['winner'] = tonumber(data[i]['winner'])
		if data[i]['draw_odds'] then
			local draw_odds = data[i]['draw_odds']
			data[i]['draw_odds'] = tonumber(draw_odds)
		end
		if data[i]['j1_odds'] then
			local j1_odds = data[i]['j1_odds']
			data[i]['j1_odds'] = tonumber(j1_odds)
		end
		if data[i]['j2_odds'] then
			local j2_odds = data[i]['j2_odds']
			data[i]['j2_odds'] = tonumber(j2_odds)
		end

		i  =  i + 1
	end
	return data
end



function Tools:initTimeFrames(configTFs, data)	

	print('Inserting Frames...')

	local TF = {}
	local tStart = 1
	local tEnd = configTFs.nTrain + configTFs.nTest
	local nconfigTFs = self:shallow_copy(configTFs)

	local i = 1
	while tEnd <= #data do
		local subdata = sportsbet.TimeFrame:sub(data, tStart, tEnd)
		
		--config of TimeFrame
		nconfigTFs.tStart = tStart
		nconfigTFs.tEnd = tEnd
		nconfigTFs.tStart = tStart

		--init of TimeFrame
		TF[i] = sportsbet.TimeFrame(nconfigTFs, subdata)

		--for next TimeFrame
		tStart = tStart + configTFs.gap
		tEnd = tEnd + configTFs.gap

		i = i + 1
	end
	return TF
end



function Tools:initTimeFrames2(configTFs, data)	

	print('Inserting Frames...')
	
	local TF = {}
	local nTrain = configTFs.nTrain
	local nTest = configTFs.nTest
	local gap = configTFs.gap
	local nValidation = configTFs.nValidation or 0
	local nconfigTFs = self:shallow_copy(configTFs)

	local i  =  1
	while #data - nTest - nTrain >=  0 do
		
		-- config of TimeFrame
		nconfigTFs.tStart = #data - nTest - nTrain - nValidation
		
		-- getting data for TimeFrame
		local subdata = sportsbet.TimeFrame:sub(data, nconfigTFs.tStart, #data)

		-- init of TimeFrame
		TF[i] = sportsbet.TimeFrame(nconfigTFs, subdata)

		i = i + 1
		nTrain = nTrain + gap
		nconfigTFs.nTrain = nTrain
	end
	return TF
end



function Tools:shallow_copy(data)
  local data2 = {}
  for k,v in pairs(data) do
    data2[k] = v
  end
  return data2
end



function Tools:list_iter (t)
	local i = 0
	local n = table.getn(t)
	return function ()
		i = i + 1
		if i <= n then return t[i] end
	end
end



function Tools:initLKTIndex(data)

	current = {}
	LKTIndex = {}
	newData = self:shallow_copy(data)

	for i=1, #newData do
		match = newData[i]
		j1 = {[0] = match.j1_id, [1] = 'j1_t'}
		j2 = {[0] = match.j2_id, [1] = 'j2_t'}
		for j in self:list_iter({j1, j2}) do
			if(j[0] ~= nil) then
				if(current[j[0]] == nil) then
					current[j[0]] = 0
					LKTIndex[j[0]] = {}
				end
				current[j[0]] = current[j[0]] + 1
				match[j[1]] = current[j[0]]
				table.insert(LKTIndex[j[0]], i)
			end
		end
	end
	return newData, LKTIndex
end



function tableGetLength(T)
	local count = 0
	for _ in pairs(T) do count = count + 1 end
	return count
end



function dateToNumber(date)

	if date == '' then return 0 end
	new = {}
	for w in date:gmatch("[^/]*") do
		table.insert(new, w)
	end
	day = new[1]
	month = new[3]
	year = new[5]:sub(1,4)

	return os.time({year=year, month=month, day=day})
end



function Tools:savePlayers(self, savePath, nameFile)

-- to do

end



function Tools:experiments(config, data, opt, log)

	if config.module_name == "MatrixFactBiModel" then mod = sportsbet.MatrixFactBiModel(config, data.train)
	elseif config.module_name == "MatrixFactSubModel" then mod = sportsbet.MatrixFactSubModel(config, data.train)
	elseif config.module_name == "RandomModel" then mod = sportsbet.RandomModel(config, data.train)
	elseif config.module_name == "RankingModel" then mod = sportsbet.RankingModel(config, data.train)
	elseif config.module_name == "TemporalIdentityModel" then mod = sportsbet.TemporalIdentityModel(config, data.train)
	elseif config.module_name == "TemporalLinearModel" then mod = sportsbet.TemporalLinearModel(config, data.train)
	elseif config.module_name == "TemporalVector" then mod = sportsbet.TemporalVector(config, data.train)
	elseif config.module_name == "FeaturesAddModel" then mod = sportsbet.FeaturesAddModel(config, data.train)
	elseif config.module_name == "FeaturesSubModel" then mod = sportsbet.FeaturesSubModel(config, data.train)
	end

	for epoch = 1, config.maxEpoch, config.stepEpoch do

		for fitStep = 1, config.stepEpoch do
			trainLoss = mod:fitStep(data.train)
		end

		--Evaluation on train data
		local trainLabels, trainProbs = mod:predict(data.train)
		local trainPrecision = mod:precisionRatio(data.train, trainLabels)
		
		--Evaluating error on test data
		local testLabels, testProbs, testLoss = mod:predict(data.test)
		local testPrecision, testRatio = mod:precisionRatio(data.test, testLabels)

		--Evaluating error on validation data
		local valiLabels, valiProbs, valiLoss = mod:predict(data.validation)
		local valiPrecision, valiRatio = mod:precisionRatio(data.validation, valiLabels)

		--Evaluating ROI on test data
		local naiveStrat = sportsbet.NaiveStrat(data.test, testLabels, 1)
		local naiveROI, naiveBank, naiveBetNumber = naiveStrat:evaluate()
		if(config.module_name ~= "RankingModel" and config.module_name ~= "RandomModel") then
			local probStrat = sportsbet.ProbStrat(data.test, testLabels, 1, testProbs)
			local kellyStrat = sportsbet.KellyStrat(data.test, testLabels, 1, testProbs)
			probROI, probBank, probBetNumber = probStrat:evaluate()
			kellyROI, kellyBank, kellyBetNumber = kellyStrat:evaluate()
		else
			probROI, probBank, probBetNumber = 0, 0, 0
		        kellyROI, kellyBank, kellyBetNumber = 0, 0, 0
		end

		--Saving logs
		log:newIteration()
		log:addValue('testRatio', testRatio)
		log:addValue('train_ratio', train_ratio)
		log:addValue('trainPrecision', trainPrecision)
		log:addValue('trainLoss', trainLoss)
		log:addValue('testLoss', testLoss)
		log:addValue('epoch', epoch)  	
		log:addValue('testPrecision', testPrecision)
		log:addValue('validationPrecision', valiPrecision)
		log:addValue('validationRatio', valiRatio)
		log:addValue('model', config.module_name)	
		log:addValue('timeFrame', idf)
		log:addValue('modconfig', mod.config)
		log:addValue('TFconfig', data.config)
		log:addValue('naiveBank', naiveBank)
		log:addValue('probBank', probBank)
		log:addValue('kellyBank', kellyBank)
		log:addValue('naiveROI', naiveROI)
		log:addValue('probROI', probROI)
		log:addValue('kellyROI', kellyROI)
		log:addValue('naiveBetNumber', naiveBetNumber)
		log:addValue('probBetNumber', probBetNumber)
		log:addValue('kellyBetNumber', kellyBetNumber)
		log:addValue('opt', opt)

	end
end
