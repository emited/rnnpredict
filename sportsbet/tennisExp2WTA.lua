opt={}
opt.Tmin={"1990-01-01","1991-01-01","1992-01-01","1993-01-01","1994-01-01","1995-01-01","1996-01-01","1997-01-01","1998-01-01","1999-01-01","2000-01-01","2001-01-01","2002-01-01","2003-01-01","2004-01-01","2004-01-01","2005-01-01","2006-01-01","2007-01-01","2008-01-01","2009-01-01","2010-01-01","2011-01-01","2012-01-01","2013-01-01","2014-01-01","2015-01-01"}
opt.Tmax={"2060-01-01"}
opt.tStart={"1990-01-01"}
opt.nTrain={20000, 30000, 40000, 50000, 60000, 70000, 80000, 100000}
opt.nTest={500}
opt.nValidation={400}
opt.shuffle={"true"}
opt.threshold={0, 5, 10, 20, 30}
opt.logPath={"./results/tennis_wta"}
opt.verbose={"false"}
opt.db={"db/wta_database"}

model_name="TennisWTABetModel"
model_parameters={
  maxEpoch={2},
  stepEpoch={1},
  nClasses = {2},
  module_name={"RandomModel", "RankingModel"}
  }

executable="exp.lua"
use_oarstat=false
max_nb_processes=200
debug=false
