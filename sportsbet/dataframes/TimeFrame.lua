local math = require 'math'
local table = require 'table'


local TimeFrame=torch.class('sportsbet.TimeFrame','sportsbet.DataFrame')


function TimeFrame:__init(config, data)

	--definining config
	self.config = {
		tStart = config.tStart,
		shuffle = config.shuffle or false,
		nTrain = config.nTrain or 1000,
		nValidation = config.nValidation or 0,
		threshold = config.threshold or 0,
		nTest = config.nTest or 100,
		dTrainStart = data[1].date,
		dTrainEnd = data[config.nTrain].date,
		dTestEnd = data[config.nTrain + config.nTest].date,
		dValidationEnd = data[config.nTrain + config.nTest + (config.nValidation or 0)].date
	}

	-- init of train, test and validation
	self.train = self:sub(data, 1, self.config.nTrain)

	self.test = self:sub(data,
		self.config.nTrain + 1,
		self.config.nTrain + self.config.nTest)

	if self.config.nValidation ~= 0 then
		self.validation = self:sub(data,
			self.config.nTrain + self.config.nTest + 1,
			self.config.nTrain + self.config.nTest + self.config.nValidation)
	end

	-- applying threshold
	if self.config.threshold > 0 then
		self.train = self:threshold(self.train, self.config.threshold)
		self.config.nTrain = #self.train
	end

	-- applying shuffle
	if self.config.shuffle then
		self.train = self:shuffle(self.train)
		self.test = self:shuffle(self.test)
		if self.config.nValidation > 0 then
			self.validation = self:shuffle(self.validation)
		end
	end	
	
end


function TimeFrame:sub(data, tStart, tEnd)

	if(tStart<1 or tStart>tEnd or tEnd>#data) then
		error('Wrong tStart or tEnd.')
	else
		subtable={}
		for i=tStart,tEnd  do 
			subtable[i-tStart+1]=data[i]
		end
	end
	return subtable
end


function TimeFrame:shuffle(data)
	data2 = sportsbet.Tools:shallow_copy(data)
	for i = 1, #data2 do
		if(math.random()>0.5) then
			if(data2[i]['winner'] == -1) then
				--permutation
				data2[i]['winner'] = 1
				local tmp = data2[i]['j1_id']
				data2[i]['j1_id'] = data2[i]['j2_id']
				data2[i]['j2_id'] = tmp
				
				if(data2[i]['j1_odds'] and data2[i]['j2_odds']) then

					local tmp = data2[i]['j1_odds']
					data2[i]['j1_odds'] = data2[i]['j2_odds']
					data2[i]['j2_odds'] = tmp
				end

			elseif(data2[i]['winner'] == 1) then
				--permutation
				data2[i]['winner'] = -1
				local tmp = data2[i]['j1_id']
				data2[i]['j1_id'] = data2[i]['j2_id']
				data2[i]['j2_id'] = tmp
				if(data2[i]['j1_odds'] and data2[i]['j2_odds']) then
					local tmp = data2[i]['j1_odds']
					data2[i]['j1_odds'] = data2[i]['j2_odds']
					data2[i]['j2_odds'] = tmp
				end
			end
		end
	end
	return data2
end


function TimeFrame:threshold(data, n)
	if n > 0 then
		nbMatchesById = {}
		for i = 1, #data do
			j1_id = data[i]['j1_id']
			j2_id = data[i]['j2_id']
			if nbMatchesById[j1_id] == nil then
				nbMatchesById[j1_id] = 1
			else
				nbMatchesById[j1_id] = nbMatchesById[j1_id] + 1
			end
			if nbMatchesById[j2_id] == nil then
				nbMatchesById[j2_id] = 1
			else
				nbMatchesById[j2_id] = nbMatchesById[j2_id] + 1
			end
		end
		ndata = {}
		iter = 1
		for i = 1, #data do
			if nbMatchesById[data[i]['j1_id']] > n and
				nbMatchesById[data[i]['j2_id']] > n then
				ndata[iter] = data[i]
				iter = iter + 1
			end
		end
		return ndata
	else
		return data
	end
end
