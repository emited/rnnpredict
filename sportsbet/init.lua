require 'torch'
sportsbet={}

--- MODULES
include('ExperimentLog.lua')
include('ExperimentLogConsole.lua')
include('ExperimentLogCSV.lua')
include('ModelsUtils.lua')

-- Ajout
include('Tools.lua')
include('Strategies.lua')

-- Modeles
include('DataFrame.lua')
include('TimeFrame.lua')

include('AbstractModel.lua')
include('RankingModel.lua')
include('ScalarModel.lua')
include('RandomModel.lua')


include('MatrixFactModel.lua')
include('MatrixFactSubModel.lua')
include('MatrixFactBiModel.lua')
include('LogitRegressionModel.lua')

include('TemporalModel.lua')
include('TemporalVectorModel.lua')
include('TemporalIdentityModel.lua')
include('TemporalLinearModel.lua')

include('FeaturesAddModel.lua')
include('FeaturesSubModel.lua')
return sportsbet
