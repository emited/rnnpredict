opt={}
opt.Tmin={"2013-01-01"}
opt.Tmax={"2060-01-01"}
opt.tStart={"1990-01-01"}
opt.nTrain={20000}
opt.nTest={60000}
opt.nValidation={200}
opt.shuffle={"true"}
opt.threshold={0}
opt.logPath={"./results/tennis_atp"}
opt.verbose={"false"}
opt.db={"db/atp_database"}

model_name="TennisATPBetModel"
model_name="TennisATPBetModel"
model_parameters={
  maxEpoch={3},
  stepEpoch={1},
  nClasses = {2},
  module_name={"MatrixFactSubModel"}
  }

executable="exp.lua"
use_oarstat=false
max_nb_processes=200
debug=false
