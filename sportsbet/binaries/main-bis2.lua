require 'sportsbet'
require 'lsqlite3'
require 'lfs'
require 'math'

plus = debug.getinfo(1).source:match("@(.*/)")
if(plus == nil) then plus = '' end
currentPath  =  lfs.currentdir() .. '/' .. plus
               
torch.setnumthreads(1)

cmd = torch.CmdLine()
cmd:text()
cmd:option('--logPath', "log/", "The directory where one wants to save the log of the experiment")
cmd:option('--verbose', "true", "if 'true' then the output is the console, else it is the log file")
cmd:text()
local opt  =  cmd:parse(arg or {})
 
--- Creation du log
if (opt.verbose == 'false') then log = sportsbet.ExperimentLogCSV(true, opt.logPath, "now") else log = sportsbet.ExperimentLogConsole(true) end
log:addFixedParameters(opt) --on y ajoute les arguments de la ligne de commande


-------------------- MAIN --------------------

math.randomseed(os.time())
math.random(); math.random(); math.random()

local path = "../../tennis_data/LudovicDenoyer_data/atp_database"


--Setting constants
local nFitSteps = 1
local nLogSteps = 200
local wD = {[0] = 0, [1] = 1e-3, [2] = 1e-2}

local configTFs = {
	Tmin = "2012-01-01",
	Tmax = "2015-03-12",
	gap = 500,
	threshold = 30,
	nTrain = 80000,
	nTest = 700,
	shuffle = true
}

-- loading DB
local data = sportsbet.Tools:loadDB(path, configTFs.Tmin, configTFs.Tmax)
local timeFrames = sportsbet.Tools:initTimeFrames(configTFs, data)

--log:addDescription('')

for idf = 2, 5 do

	for jSize = 10, 100, 10 do
		for k = 0, 100 do
			batchSize = math.pow(8, k)
			if(batchSize > (#(timeFrames[idf].train))/100) then break end
			for w = 0, 0 do
				weightDecay = wD[w]
				for i = 1, 1 do
					--Initializing model(s)
					local configModel = {
						jSize = jSize,
						batchSize = batchSize,
						weightDecay = weightDecay,
						nClasses = 2,
						learningRate = 0.01,
						learningRateDecay = 0,
						momentum = 0
					}
					local mod = sportsbet.MatrixFactModel(configModel, timeFrames[idf].train)
					print('Config Model')
					print(configModel)
					print('Time Frame')
					print(timeFrames[idf].config, '\n')

					for logStep = 1, nLogSteps do
						-- fit	
						for fitStep = 1, nFitSteps do
							trainLoss = mod:fitStep(timeFrames[idf].train)
						end

						--Evaluation on train data
						local trainLabels, trainProbs = mod:predict(timeFrames[idf].train)
						local trainPrecision = mod:precisionRatio(timeFrames[idf].train, trainLabels)
						
						--Evaluating error on test data
						local testLabels, testProbs, testLoss = mod:predict(timeFrames[idf].test)
						local testPrecision, testRatio = mod:precisionRatio(timeFrames[idf].test, testLabels)

						-- Evaluating ROI on test data
						local naiveStrat = sportsbet.NaiveStrat(timeFrames[idf].test, testLabels, 1)
						local probStrat = sportsbet.ProbStrat(timeFrames[idf].test, testLabels, 1, testProbs)
						local kellyStrat = sportsbet.KellyStrat(timeFrames[idf].test, testLabels, 1, testProbs)
						local naiveROI, naiveBank = naiveStrat:evaluate()
						local probROI, probBank = probStrat:evaluate()
						local kellyROI, kellyBank = kellyStrat:evaluate()
						
						print('ROI', probROI, naiveROI, kellyROI)
						print('Bank', probBank, naiveBank, kellyBank)
						print('testPrecision', testPrecision)
						print('trainPrecision', trainPrecision)
						print('trainLoss', trainLoss)

						--Saving logs
						log:newIteration()
						log:addValue('testRatio', testRatio)
						log:addValue('train_ratio', train_ratio)
						log:addValue('trainPrecision', trainPrecision)
						log:addValue('trainLoss', trainLoss)
						log:addValue('testLoss', testLoss)
						log:addValue('epoch', logStep*nFitSteps)  	
						log:addValue('testPrecision', testPrecision)
						log:addValue('model', configModel)	
						log:addValue('timeFrame', idf)
						log:addValue('modConfig', mod.params)
						log:addValue('TFConfig', timeFrames[idf].params)
						log:addValue('naiveBank', naiveBank)
						log:addValue('probBank', probBank)
						log:addValue('kellyBank', kellyBank)
						log:addValue('naiveROI', naiveROI)
						log:addValue('probROI', probROI)
						log:addValue('kellyROI', kellyROI)
				  
						print('Version', i, 'Epoch',
						logStep*nFitSteps, '\n')
					end
				end
			end
		end
	end
end

