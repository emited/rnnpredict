require 'sportsbet'
require 'lsqlite3'
require 'lfs'
require 'math'

plus = debug.getinfo(1).source:match("@(.*/)")
if(plus == nil) then plus = '' end
currentPath  =  lfs.currentdir() .. '/' .. plus
               
torch.setnumthreads(1)

cmd = torch.CmdLine()
cmd:text()
cmd:option('--logPath', "log/", "The directory where one wants to save the log of the experiment")
cmd:option('--verbose', "true", "if 'true' then the output is the console, else it is the log file")
cmd:text()
local config  =  cmd:parse(arg or {})
 
--- Creation du log
if (config.verbose == 'false') then log = sportsbet.ExperimentLogCSV(true, config.logPath, "now") else log = sportsbet.ExperimentLogConsole(true) end
log:addFixedParameters(config) --on y ajoute les arguments de la ligne de commande

-------------------- MAIN --------------------

math.randomseed(os.time())
math.random(); math.random(); math.random()

--local path = "../../foot_data/foot_database"
local path = "../../tennis_data/LudovicDenoyer_data/atp_database"

--Setting constants
local nFitSteps = 1
local nLogSteps = 20


local configTfs = {
	Tmin = "2011-10-08",
	Tmax = "2014-20-05",
	gap = 500,
	threshold = 30,
	nTrain = 60050,
	nTest = 500,
	nValidation = 400,
	shuffle = true
}

local configModel = {
		jSize = 30,
		nClasses = 2,
		learningRate = 1e-3,
		learningRateDecay = 3e-5,
		batchSize = 50,
		weightDecay  = 0,
		momentum = 0,
		initValues = .01,
		alg = 'adagrad',
		lambda = 10.5,
}

--local params = {"j1_id", "j2_id", "winner", "date", "j1_odds", "j2_odds", "draw_odds"}

-- loading DB
local data = sportsbet.Tools:loadDB(path, configTfs.Tmin, configTfs.Tmax, params)

local timeFrames = sportsbet.Tools:initTimeFrames2(configTfs, data)

--log:addDescription	('')
for idf = 1, 3 do

	--Initializing model
	local mod = sportsbet.MatrixFactBiModel(configModel, timeFrames[idf].train)
	

	for logStep = 1, nLogSteps do
		
		--fit
		for fitStep = 1, nFitSteps do
			trainLoss = mod:fitStep(timeFrames[idf].train)
		end

		--Evaluation on train data
		local trainLabels, trainProbs = mod:predict(timeFrames[idf].train)
		local trainPrecision = mod:precisionRatio(timeFrames[idf].train, trainLabels)
		
		--Evaluating error on test data
		local testLabels, testProbs, testLoss = mod:predict(timeFrames[idf].test)
		local testPrecision, testRatio = mod:precisionRatio(timeFrames[idf].test, testLabels)

		--Evaluating error on validation data
		local valiLabels, valiProbs, valiLoss = mod:predict(timeFrames[idf].validation)
		local valiPrecision, valiRatio = mod:precisionRatio(timeFrames[idf].validation, valiLabels)


		--Evaluating ROI on test data
		local naiveStrat = sportsbet.NaiveStrat(timeFrames[idf].test, testLabels, 1)
		local probStrat = sportsbet.ProbStrat(timeFrames[idf].test, testLabels, 1, testProbs)
		local kellyStrat = sportsbet.KellyStrat(timeFrames[idf].test, testLabels, 1, testProbs)
		local naiveROI, naiveBank = naiveStrat:evaluate()
		local probROI, probBank = probStrat:evaluate()
		local kellyROI, kellyBank = kellyStrat:evaluate()


		--prints
		print('ROI', probROI, naiveROI, kellyROI)
		print('Bank', probBank, naiveBank, kellyBank)
		print('testPrecision', testPrecision)
		print('trainPrecision', trainPrecision)
		print('trainLoss', trainLoss)
		print('validationPrecision',valiPrecision)

		--Saving logs
		log:newIteration()
		log:addValue('testRatio', testRatio)
		log:addValue('train_ratio', train_ratio)
		log:addValue('trainPrecision', trainPrecision)
		log:addValue('trainLoss', trainLoss)
		log:addValue('testLoss', testLoss)
		log:addValue('epoch', logStep*nFitSteps)  	
		log:addValue('testPrecision', testPrecision)
		log:addValue('validationPrecision', valiPrecision)
		log:addValue('timeFrame', idf)
		log:addValue('modconfig', mod.config)
		log:addValue('TFconfig', timeFrames[idf].config)
		log:addValue('naiveBank', naiveBank)
		log:addValue('probBank', probBank)
		log:addValue('kellyBank', kellyBank)
		log:addValue('naiveROI', naiveROI)
		log:addValue('probROI', probROI)
		log:addValue('kellyROI', kellyROI)

		print(logStep*nFitSteps)

	end

	--mod:savePlayers(config.logPath, 'testrepr2.csv')
end
