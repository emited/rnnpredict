-- opt is the table of parameters

require 'sportsbet'
require 'nn'
require 'gnuplot'

cmd=torch.CmdLine()
cmd:text()
cmd:option('--option_file','','the name of the lua file for experiments')
cmd:text()
opt = cmd:parse(arg or {})
assert(opt.option_file~="")
require(opt.option_file)
print(opt)

params={"j1_id", "j2_id", "winner", "date", "j1_odds", "j2_odds", "surface", "j1_age", "j2_age", "j1_country", "j2_country", "level", "round"}

local games = sportsbet.Tools:loadDB(opt.db,opt.Tmin,opt.Tmax, params)
print("Number of games is "..#games)
games = sportsbet.TimeFrame(opt, games) 
print("Number of games (after pruning) is "..(#games.train+#games.test))
local flag=true

if (opt.verbose=='false') then log=sportsbet.ExperimentLogCSV(true,opt.logPath,"now") else log=sportsbet.ExperimentLogConsole(true)
end
log:addFixedParameters(opt)

sportsbet.Tools:experiments(model_parameters, games, opt, log)
