require 'optim'
require 'nn'
require 'nngraph'

 
 local NLLBetModel = torch.class('betdnn.NLLBetModel','betdnn.BetModel'); 

function NLLBetModel:__init(nb_categories,parameters)
  assert(nb_categories==2)
  betdnn.BetModel.__init(self,nb_categories)  
  
  if (parameters~=nil) then
    self.parameters=parameters
    assert(parameters.maxEpoch~=nil)
    assert(parameters.learningRate~=nil)
    assert(parameters.stepEpoch~=nil)
    assert(parameters.N~=nil)
    assert(parameters.size_minibatch~=nil)
    assert(parameters.cuda~=nil)
    assert(parameters.stdv~=nil)
    assert(parameters.optim_method~=nil)
    end
end

function NLLBetModel:init_train(games)
  ------------------------------------------------------------------------
  print("Indexing players")
  self.games=games
  self.ngames=#games
  
  self.players={}; self.nb_players=0
  local ys={}
  for i=1,#games do
    if (self.players[games[i].p1]==nil) then      
      self.players[games[i].p1]=self.nb_players+1      
      self.nb_players=self.nb_players+1
    end
    if (self.players[games[i].p2]==nil) then      
      self.players[games[i].p2]=self.nb_players+1
      self.nb_players=self.nb_players+1
    end        
  end
  print("\tFound "..self.nb_players.." players")
  ------------------------------------------------------------------------print("-- Creating embeddings for "..self.nb_players.." players")
  self.zs=nn.LookupTable(self.nb_players,self.parameters.N)
  self.zs:reset(self.parameters.stdv)    
  
  ------------------------------------------------------------------------print("Creating model")
    self.model=nil
  do
    local in1=nn.Identity()()
    local in2=nn.Identity()()
    local l1=nn.Linear(self.parameters.N,self.parameters.N)(in1)
    local l2=nn.Linear(self.parameters.N,self.parameters.N)(in2)
--    l1=nn.Tanh()(l1)
--    l2=nn.Tanh()(l2)
    local add=nn.CAddTable()({l1,l2})
    add=nn.Tanh()(add)
    local l3=nn.Linear(self.parameters.N,2)(add)
    self.model=nn.gModule({in1,in2},{l3})
  end
  self.model:reset(self.parameters.stdv)
  self.softmax=nn.SoftMax()
  self.criterion=nn.CrossEntropyCriterion()
  
  ------------------------------------------------------------------- creating minbatches
  
  
  self.inputs={}
  self.outputs={}
  
  local sb=self.parameters.size_minibatch
  self.nb_batches=math.floor(self.ngames/sb)+1
  --if (ngames==sb) then nb_batches=1 end
  print("Creating "..self.nb_batches.." batches of size "..sb)
  for i=1,self.nb_batches do
    self.inputs[i]={}
    self.inputs[i].team1=torch.Tensor(sb)
    self.inputs[i].team2=torch.Tensor(sb)
    self.outputs[i]={}
    self.outputs[i].vs1=torch.Tensor(sb,1)
    self.outputs[i].vs2=torch.Tensor(sb,1)
    for k=1,sb do
      local idx=math.random(self.ngames)
      local i1=self.players[games[idx].p1]
      local i2=self.players[games[idx].p2]
      self.inputs[i].team1[k]=i1
      self.inputs[i].team2[k]=i2
      self.outputs[i].vs1[k][1]=games[idx].result
      if (games[idx].result==1) then self.outputs[i].vs2[k][1]=2 else self.outputs[i].vs2[k][1]=1 end
    end
  end
  ----------------------------- CUDA ? 
  if (self.parameters.cuda=="true") then
    print("Moving to everything to GPU...")
    require 'cunn';
    self.model:cuda()
    self.criterion:cuda()
    self.zs:cuda()
    for i=1,self.nb_batches do
      self.inputs[i].team1=self.inputs[i].team1:cuda()
      self.inputs[i].team2=self.inputs[i].team2:cuda()
      self.outputs[i].vs1=self.outputs[i].vs1:cuda()
      self.outputs[i].vs2=self.outputs[i].vs2:cuda()
    end
  end
      
  ------------------------------------------------------------------------
  self.params, self.grad = betdnn.ModelsUtils():combine_all_parameters(self.zs,self.model)
  print("Number of parameters is "..self.params:size(1))
  
  local clone_zs=betdnn.ModelsUtils():clone_many_times(self.zs,2)
  self.zs_1=clone_zs[1]
  self.zs_2=clone_zs[2]
  
  --------------------------------------------------------------------------------------
  self.feval = function(params_new)
     if self.params ~= params_new then
        self.params:copy(params_new)
     end

     -- select a new training sample
     local _nidx_ = math.random(self.nb_batches)
     
     --if _nidx_ > opt.nb_minibatches then _nidx_ = 1 end
     self.grad:zero()
     local loss=0
     do
       
      local input1=self.inputs[_nidx_].team1
      local input2=self.inputs[_nidx_].team2
       local y = self.outputs[_nidx_].vs1
       local _z1=self.zs_1:forward(input1)
       local _z2=self.zs_2:forward(input2)
       local out=self.model:forward({_z1,_z2})
       local loss1=self.criterion:forward(out,y)
       local delta=self.criterion:backward(out,y)     
       delta=self.model:backward({_z1,_z2},delta)     
       self.zs_1:backward(input1,delta[1])
       self.zs_2:backward(input2,delta[2])
       loss=loss+loss1
    end
     
     do
       
      local input1=self.inputs[_nidx_].team2
      local input2=self.inputs[_nidx_].team1
       local y = self.outputs[_nidx_].vs2
       local _z1=self.zs_1:forward(input1)
       local _z2=self.zs_2:forward(input2)
       local out=self.model:forward({_z1,_z2})
       local loss2=self.criterion:forward(out,y)
       local delta=self.criterion:backward(out,y)     
       delta=self.model:backward({_z1,_z2},delta)     
       self.zs_1:backward(input1,delta[1])
       self.zs_2:backward(input2,delta[2])
       loss=loss+loss2
      end
     
     
     return loss,self.grad
  end
  
  self.sgd_params = {
   learningRate =  self.parameters.learningRate,
   learningRateDecay = 0,
   weightDecay = 0,
   momentum = 0.9
  }

  self.nag_params = {
    learningRate =  self.parameters.learningRate,
    learningRateDecay = 1e-4,
    weightDecay = 0.0,
    momentum = 0.9
  }
  --
  self.total_iteration=0
  print("Training ready...")  
end  
  
function NLLBetModel:train()
  if (self.total_iteration>=self.parameters.maxEpoch) then return nil end
----------------------------------------------------------------------
local final_loss=0
  for iteration=1,self.parameters.stepEpoch do
    
    local current_loss=0
    for i = 1,self.nb_batches do
      if (self.parameters.optim_method=="sgd") then
        _,fs = optim.sgd(self.feval,self.params,self.sgd_params)      
      else
        _,fs = optim.nag(self.feval,self.params,self.nag_params) 
      end
        current_loss = current_loss + fs[1]      
     end
     current_loss=current_loss/self.nb_batches
          
     print("At "..iteration..", loss is "..current_loss)       
     final_loss=current_loss
  end
  self.total_iteration=self.total_iteration+self.parameters.stepEpoch    
  return final_loss
end

function NLLBetModel:predict(p1,p2)  
  
  local i1=self.players[p1]
  local i2=self.players[p2]
  
  if ((i1==nil) or (i2==nil)) then return nil end
  local x1=torch.Tensor(1):fill(i1)
  local x2=torch.Tensor(1):fill(i2)
  if (self.parameters.cuda=="true") then
    x1=x1:cuda()
    x2=x2:cuda()
  end
  
  local _z1=self.zs:forward(x1):clone()
  local _z2=self.zs:forward(x2):clone()
  local _out=self.model:forward({_z1,_z2})
  _out=self.softmax:forward(_out)
  return( {_out[1][1],_out[1][2]} )
end

function NLLBetModel:save(filename)
  print("Saving NLLBetModel in "..filename)
  local file=torch.DiskFile(filename,"w")
  file:ascii()
  file:writeObject(self.parameters)  
  file:writeObject(self.players)
  file:writeObject(self.zs)
  file:writeObject(self.model)
  file:writeObject(self.softmax)  
  file:close()
end

function NLLBetModel:load(filename)
  print("Loading NLLBetModel from "..filename)
    local file=torch.DiskFile(filename,"r")
  file:ascii()
  self.parameters=file:readObject()
  if (self.parameters.cuda=="true") then require 'cutorch' end
  self.players=file:readObject()
  self.zs=file:readObject()
  self.model=file:readObject()
  self.softmax=file:readObject()
  file:close()
end
