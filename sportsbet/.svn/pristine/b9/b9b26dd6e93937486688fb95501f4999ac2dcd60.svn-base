require 'optim'
require 'nn'
require 'nngraph'

 
 local RecurrentBetModelBatch = torch.class('betdnn.RecurrentBetModelBatch','betdnn.BetModel'); 

function RecurrentBetModelBatch:__init(nb_categories,parameters)
  assert(nb_categories==2)
  betdnn.BetModel.__init(self,nb_categories)  
  self.parameters=parameters
  assert(parameters.maxEpoch~=nil)
  assert(parameters.learningRate~=nil)
  assert(parameters.stepEpoch~=nil)
  assert(parameters.N~=nil)
  assert(parameters.stdv~=nil)
  assert(parameters.lambda_prediction~=nil)
  assert(parameters.lambda_dynamic~=nil)
  assert(parameters.size_minibatch~=nil)
end

function RecurrentBetModelBatch:shuffled(tab)
    local n, order, res = #tab, {}, {}
    for i=1,n do order[i] = { rnd = math.random(), idx = i } end
    table.sort(order, function(a,b) return a.rnd < b.rnd end)
    for i=1,n do res[i] = tab[order[i].idx] end
    return res
end

function RecurrentBetModelBatch:init_train(games)
  ------------------------------------------------------------------------
  print("Indexing players")
  self.games=games
  self.ngames=#games
  
  self.players={}; self.nb_players=0
  local ys={}
  for i=1,#games do
    if (self.players[games[i].p1]==nil) then      
      self.players[games[i].p1]=self.nb_players+1      
      self.nb_players=self.nb_players+1
    end
    if (self.players[games[i].p2]==nil) then      
      self.players[games[i].p2]=self.nb_players+1
      self.nb_players=self.nb_players+1
    end        
  end
  print("\tFound "..self.nb_players.." players")
  ------------------------------------------------------------------------print("-- Creating embeddings for "..self.nb_players.." players")
  print("Creating embeddings")
  
  self.last_index_player={}
  for p,_ in pairs(self.players) do self.last_index_player[p]=self.players[p] end
  local next_index=self.nb_players+1
  self.matches={}
  for i=1,#games do 
    self.matches[i]={}
    local p1=games[i].p1
    local p2=games[i].p2
    self.matches[i].p1_from=self.last_index_player[p1]
    self.matches[i].p2_from=self.last_index_player[p2]
    self.matches[i].p1_to=next_index; self.last_index_player[p1]=next_index
    self.matches[i].p2_to=next_index+1; self.last_index_player[p2]=next_index+1
    self.matches[i].result=games[i].result
    next_index=next_index+2
  end
  self.zs=nn.Sequential():add(
    nn.LookupTable(next_index-1,self.parameters.N)
    ):add(
    nn.Normalize(2)
  )
   self.zs:reset(self.parameters.stdv)
  
  ---------------------------------- MINIBATCHES
  print("Creating minibatches")
  self.mbatch={}
  do
    local ggames=self:shuffled(self.matches)  
    local pos=1
    while(pos<=#ggames) do
      local ntoadd=math.min(self.parameters.size_minibatch,#ggames-pos+1)
      local bpos=#self.mbatch+1
      self.mbatch[bpos]={}
      self.mbatch[bpos].p1=torch.Tensor(ntoadd)
      self.mbatch[bpos].p2=torch.Tensor(ntoadd)
      self.mbatch[bpos].y=torch.Tensor(ntoadd)
      for i=1,ntoadd do
        local pggames=pos+i-1
        self.mbatch[bpos].p1[i]=ggames[pggames].p1_from
        self.mbatch[bpos].p2[i]=ggames[pggames].p2_from
        self.mbatch[bpos].y[i]=ggames[pggames].result
      end
      pos=pos+ntoadd
    end
  end
  self.nb_mbatch=#self.mbatch
  print("-->"..self.nb_mbatch)
  print("Creating minibatches for relations...")
  self.rbatch={}
    do
    local ggames=self:shuffled(self.matches)  
    local pos=1
    while(pos<=#ggames) do
      local ntoadd=math.min(self.parameters.size_minibatch,#ggames-pos+1)
      local bpos=#self.rbatch+1
      self.rbatch[bpos]={}
      self.rbatch[bpos].p_from=torch.Tensor(ntoadd*2)
      self.rbatch[bpos].p_to=torch.Tensor(ntoadd*2)
      self.rbatch[bpos].p_input=torch.Tensor(ntoadd*2,1)
      self.rbatch[bpos].y=torch.Tensor(ntoadd*2,1):fill(0)
      
      for i=1,ntoadd do
        local pggames=pos+i-1
        self.rbatch[bpos].p_from[2*i-1]=ggames[pggames].p1_from
        self.rbatch[bpos].p_to[2*i-1]=ggames[pggames].p1_to
        if (ggames[pggames].result==1) then self.rbatch[bpos].p_input[2*i-1][1]=1 else self.rbatch[bpos].p_input[2*i-1][1]=-1 end
        
        self.rbatch[bpos].p_from[2*i]=ggames[pggames].p2_from
        self.rbatch[bpos].p_to[2*i]=ggames[pggames].p2_to
        if (ggames[pggames].result==1) then self.rbatch[bpos].p_input[2*i][1]=-1 else self.rbatch[bpos].p_input[2*i][1]=1 end
      end
      pos=pos+ntoadd
    end
  end
  self.nb_rbatch=#self.rbatch
  print("-->"..self.nb_rbatch)
  
  print("Creating models")
  ------------------------------------------------------------------------ print("Creating model")
  self.model=nil
  do
    self.predictive_model=nil
    do        
      local in1=nn.Identity()()
      local in2=nn.Identity()()
      local l1=nn.Linear(self.parameters.N,self.parameters.N)(in1)
      local l2=nn.Linear(self.parameters.N,self.parameters.N)(in2)
      local add=nn.CAddTable()({l1,l2})
      add=nn.Tanh()(add)
      local l3=nn.Linear(self.parameters.N,2)(add)
      self.predictive_model=nn.gModule({in1,in2},{l3})
      self.predictive_model:reset(stdv)
    end    
  
    self.dynamic_models={}
    self.residu=nn.Sequential():add(nn.Linear(self.parameters.N,self.parameters.N)):add(nn.Tanh())    
    do
--      local inn=nn.Identity()()
--      local r=self.residu(inn)
--      local add=nn.CAddTable()({inn,r})
--      self.dynamic_models[1]=nn.gModule({inn},{add})
      self.dynamic_models[1]=nn.Identity()
    end
    do
--      local inn=nn.Identity()()
--      local r=self.residu(inn)
--      local add=nn.CAddTable()({inn,r})
--      add=nn.MulConstant(-1)(add)
--      self.dynamic_models[2]=nn.gModule({inn},{add})
      self.dynamic_models[2]=nn.Identity()
    end
    self.dynamic_model=nil
    do
      local inn1=nn.Identity()()
      local inn2=nn.Identity()()
      local p=nn.PairwiseDistance(2)({inn1,inn2})
      self.dynamic_model=nn.gModule({inn1,inn2},{p})
    end
  end
  
  self.predictive_model:reset(self.parameters.stdv)
  self.dynamic_model:reset(self.parameters.stdv)
  self.softmax=nn.SoftMax()
  self.predictive_criterion=nn.CrossEntropyCriterion()
  self.dynamic_criterion=nn.MSECriterion()
  self.total_iteration=0
  
  --------------- CUDA -------
  if (self.parameters.cuda=="true") then
    require 'cutorch'
    require 'cunn'
    print("Moving data to CUDA")
    for i=1,self.nb_mbatch do
      self.mbatch[i].p1=self.mbatch[i].p1:cuda()
      self.mbatch[i].p2=self.mbatch[i].p2:cuda()
      self.mbatch[i].y=self.mbatch[i].y:cuda()
    end
    for i=1,self.nb_rbatch do
      self.rbatch[i].p_from=self.rbatch[i].p1:cuda()
      self.rbatch[i].p_to=self.rbatch[i].p2:cuda()
      self.rbatch[i].y=self.rbatch[i].y:cuda()
    end
    print("Moving model to CUDA")
    self.zs:cuda()
    self.dynamic_model:cuda()
    self.predictive_model:cuda()
    self.softmax:cuda()
    self.predictive_criterion:cuda()
    self.dynamic_criterion:cuda()
  end
 ------------------------------------------------------------------------
  self.params, self.grad = betdnn.ModelsUtils():combine_all_parameters(self.zs,self.predictive_model,self.dynamic_model)
  print("Number of parameters is "..self.params:size(1))
  
  local clone_zs=betdnn.ModelsUtils():clone_many_times(self.zs,2)
  self.zs_1=clone_zs[1]
  self.zs_2=clone_zs[2]
  
  self.predictive_loss=0
  self.dynamic_loss=0
  
  --------------------------------------------------------------------------------------
  self.feval = function(params_new)
     if self.params ~= params_new then
        self.params:copy(params_new)
     end

     -- select a new training sample
     --if _nidx_ > opt.nb_minibatches then _nidx_ = 1 end
        self.grad:zero()
       local loss=0
       do
        local _nidx_ = math.random(self.nb_mbatch)
     
        local input1=self.mbatch[_nidx_].p1
        local input2=self.mbatch[_nidx_].p2
         local y = self.mbatch[_nidx_].y
         
         local _z1=self.zs_1:forward(input1)
         local _z2=self.zs_2:forward(input2)
         local out=self.predictive_model:forward({_z1,_z2})
         local loss1=self.predictive_criterion:forward(out,y)
         local delta=self.predictive_criterion:backward(out,y)
         delta:mul(self.parameters.lambda_prediction)
         delta=self.predictive_model:backward({_z1,_z2},delta)     
         self.zs_1:backward(input1,delta[1])
         self.zs_2:backward(input2,delta[2])
         self.predictive_loss=self.predictive_loss+loss1
         loss=self.parameters.lambda_prediction*loss1
      end
      
      do
        local _nidx_ = math.random(self.nb_rbatch)
     
        local input1=self.rbatch[_nidx_].p_from
        local input2=self.rbatch[_nidx_].p_to
        local y=self.rbatch[_nidx_].y
         
         local _z1=self.zs_1:forward(input1)
         local _z2=self.zs_2:forward(input2)
         local out=self.dynamic_model:forward({_z1,_z2})
         local loss1=self.dynamic_criterion:forward(out,y)
         local delta=self.dynamic_criterion:backward(out,y)
         delta:mul(self.parameters.lambda_dynamic)
         delta=self.dynamic_model:backward({_z1,_z2},delta)     
         self.zs_1:backward(input1,delta[1])
         self.zs_2:backward(input2,delta[2])
         loss=loss+self.parameters.lambda_dynamic*loss1
         self.dynamic_loss=self.dynamic_loss+loss1
      end
           
      return loss,self.grad
           
  end
  
  self.sgd_params = {
   learningRate =  self.parameters.learningRate,
   learningRateDecay = 0,
   weightDecay = 0,
   momentum = 0.9
  }

  self.nag_params = {
    learningRate =  self.parameters.learningRate,
    learningRateDecay = 1e-4,
    weightDecay = 0.0,
    momentum = 0.9
  }
  --
  self.total_iteration=0
  print("Training ready...")  
end  
  
function RecurrentBetModelBatch:train()
  print("Train")
  if (self.total_iteration>=self.parameters.maxEpoch) then return nil end
----------------------------------------------------------------------
local final_loss=0
  for iteration=1,self.parameters.stepEpoch do
    self.predictive_loss=0
    self.dynamic_loss=0
    for i = 1,self.nb_mbatch do
      if (self.parameters.optim_method=="sgd") then
        _,fs = optim.sgd(self.feval,self.params,self.sgd_params)      
      else
        _,fs = optim.nag(self.feval,self.params,self.nag_params) 
      end
     end
     self.predictive_loss=self.predictive_loss/self.nb_mbatch
     self.dynamic_loss=self.dynamic_loss/self.nb_mbatch
    local leloss=self.parameters.lambda_prediction*self.predictive_loss+self.parameters.lambda_dynamic*self.dynamic_loss
     print("At "..iteration..", loss = "..leloss.."  ("..self.predictive_loss.." ; "..self.dynamic_loss..")")       
     final_loss=leloss
  end
  self.total_iteration=self.total_iteration+self.parameters.stepEpoch    
  return final_loss
end
  
  
function RecurrentBetModelBatch:predict(p1,p2)  
  
  local i1=self.last_index_player[p1]
  local i2=self.last_index_player[p2]
  if ((i1==nil) or (i2==nil)) then return nil end
   
  local x1=torch.Tensor(1):fill(i1)
  local x2=torch.Tensor(1):fill(i2)
  if (self.parameters.cuda=="true") then
    x1=x1:cuda()
    x2=x2:cuda()
  end
  
  local _z1=self.zs:forward(x1):clone()
  local _z2=self.zs:forward(x2):clone()
  local _out=self.predictive_model:forward({_z1,_z2})
  _out=self.softmax:forward(_out)
  return( {_out[1][1],_out[1][2]} )
end
