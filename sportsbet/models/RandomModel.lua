local math = require 'math'


local RandomModel = torch.class('sportsbet.RandomModel','sportsbet.AbstractModel')



function RandomModel:__init(config)
	self.config = config
end 



-- Do nothing
function RandomModel:fitStep(xdata,ydata)
end



function RandomModel:predict(xdata)
	local labels = {}
	local classes = {-1, 1, 0}
  for i = 1,#xdata do
  	local randIdx = torch.randperm(self.config.nClasses)
  	labels[i] = classes[randIdx[1]]
  end
  return labels
end
