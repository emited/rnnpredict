local AbstractModel = torch.class('sportsbet.AbstractModel'); 

function AbstractModel:__init(params)
  self.params = params
end

function AbstractModel:fitStep()
	error('Error! this is an abstract method.')
end


function AbstractModel:score(pred,labels)

	sum = 0.

	if #pred ~= #labels then
		error('pred is size '..#pred..', and labels is size '..#labels..'.\n')
	end
	for i = 1, #pred do
		if pred[i]['winner'] == labels[i] then
			sum = sum+1.
		end
	end
	return sum/#pred
end


function AbstractModel:predict()
	error('Error! this is an abstract method.')
end


function AbstractModel:precisionRatio(pred,labels)

	if #pred ~= #labels then
		error('pred is size '..#pred..', and labels is size '..#labels..'.\n')
	end
	local score = 0.
	local total = 0.
	for i = 1, #pred do
		if labels[i] ~= 'nil' then --not missing
			if pred[i]['winner'] == labels[i] then
				score = score + 1.
			end
			total = total + 1.
		end
	end
	if score == 0 and total == 0 then
		return 0
	else
		return score/total, total/#labels
	end
end


function AbstractModel:recall(pred,labels)

	if(#pred ~= #labels) then
		error('pred is size '..#pred..', and labels is size '..#labels..'.\n')
	end
	local score = 0.
	local total = 0.
	for i = 1, #pred do
		if pred[i]['winner'] == -1 then
			if labels[i] == -1 then
				score = score + 1.
			end
			total = total + 1.
		end
	end
	if score == 0 and total == 0 then
		return 0
	else
		return score/total
	end
end


function AbstractModel:fscore(pred,labels,beta)

	local prec = self:precision_ratio(pred,labels)
	local rec = self:recall(pred,labels)
	if prec == 0 and rec == 0 then
		return 0
	else
		return (1+beta*beta)*prec*rec/(beta*beta*prec+rec)
	end
end
