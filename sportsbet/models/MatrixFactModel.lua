require 'math'
require 'nn'	
require 'nngraph'
require 'optim'

local MatrixFactModel = torch.class('sportsbet.MatrixFactModel', 'sportsbet.AbstractModel')


function MatrixFactModel:__init(config, trainData)
	
	self.config = {
		jSize = config.jSize or 15,
		nClasses = config.nClasses or 2,
		learningRate = config.learningRate or 0.01,
		learningRateDecay = config.learningRateDecay or 1e-4,
		weightDecay = config.weightDecay or 0,
		momentum = config.momentum or 0,
		batchSize = config.batchSize or 15,
		initValues = config.initValues or 0.1,
		alg = config.alg or 'sgd'
	}

end



function initLKTIndex(data)

	index = {}
	local iter = 1

	for i = 1,#data do
		j1_id = data[i]['j1_id']
		j2_id = data[i]['j2_id']
		if index[j1_id] == nil then
			index[j1_id] = iter
			iter = iter+1
		end
		if index[j2_id] == nil then
			index[j2_id] = iter
			iter = iter+1
		end
	end
	return index, iter
end



function MatrixFactModel:fitStep(data)
	
	local batchSize = self.config.batchSize

	if batchSize > #data then
		error('batchSize > data.')
	end

	local idxmatch = torch.randperm(#data)
	local err = 0

	for cursor = 1, #data, batchSize do

		-- initializing batch
		local inputs = torch.Tensor(math.min(batchSize, #data - cursor  + 1), 2)
		local targets = torch.Tensor(math.min(batchSize, #data - cursor + 1))

		-- filling the batch
		local k = 1
		for i = cursor, math.min(cursor + batchSize -1, #data) do
			local match = data[idxmatch[i]]
			inputs[k][1] = self.LKTIndex[match['j1_id']]
			inputs[k][2] = self.LKTIndex[match['j2_id']]

			if self.config.nClasses == 2 then
				local idx = {[-1] = 1, [1] = 2}
				targets[k] = idx[match['winner']]
			elseif self.config.nClasses == 3 then 
				local idx = {[-1] = 1, [0] = 2, [1] = 3}
				targets[k] = idx[match['winner']]
			end

			k = k + 1
		end

		-- evaluating loss and gradient of loss
		local feval = function(x)
			--collectgarbage()

			if x ~= parameters then
				parameters:copy(x)
			end

			gradParameters:zero()

			local preds = self.model:forward(inputs)
			local f = self.criterion:forward(preds, targets)

			local df_do = self.criterion:backward(preds, targets)
			self.model:backward(inputs, df_do)
			return f, gradParameters
		end


		
		alg_params = {
					learningRate = self.config.learningRate * self.config.batchSize,
					learningRateDecay = self.config.learningRateDecay * self.config.batchSize,
					weightDecay = self.config.weightDecay,
					momentum = self.config.momentum
		}

		if self.config.alg == 'sgd' then
			_, fs = optim.sgd(feval, parameters, alg_params)
		elseif self.config.alg == 'adagrad' then
			_, fs = optim.adagrad(feval, parameters, alg_params)
		elseif self.config.alg == 'adam' then
			_, fs = optim.adam(feval, parameters, alg_params)
		elseif self.config.alg == 'nag' then
			_, fs = optim.nag(feval, parameters, alg_params)
		end

		err = err + fs[1]

	end
	return err/math.floor(#data/batchSize)
end




function MatrixFactModel:predict(data)
	local k = 1
	local matchIdx = {}
	local invMatchIdx = {}

	--selecting matches to predict, if both players are in LKTIndex.
	for i =1, #data do
		local match = data[i]
		if self.LKTIndex[match['j1_id']] and self.LKTIndex[match['j2_id']] then
			matchIdx[k] = i
			invMatchIdx[i] = k
			k = k + 1
		end
	end
	
	-- putting matches in a torch.Tensor, if they are any.
	if k > 1 then

		j1j2 = torch.Tensor(k-1, 2)
		winners = torch.Tensor(k-1)
		for i = 1, k-1 do
			local match = data[matchIdx[i]]
			j1j2[i][1] = self.LKTIndex[match['j1_id']]
			j1j2[i][2] = self.LKTIndex[match['j2_id']]
			winners[i] = match['winner']
		end

		-- guetting model results
		local probsTensor = self.model:forward(j1j2, winners)

		-- extracting max and argmax
		maxprob, maxindex = torch.max(probsTensor,2)

	end

	if self.config.nClasses == 2 then
		invIndex = {-1, 1}
	elseif self.config.nClasses == 3 then
		invIndex = {-1, 0, 1}
	end

	-- filling tables of preds and probs
	local preds, probs = {}, {}
	for i = 1, #data do
		if invMatchIdx[i] then
			preds[i] = invIndex[maxindex[invMatchIdx[i]][1]]
			probs[i] = maxprob[invMatchIdx[i]][1]
		else
			preds[i] = 'nil'
			probs[i] = 'nil'
		end
	end
	return preds, probs
end
