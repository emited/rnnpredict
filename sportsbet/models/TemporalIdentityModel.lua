require 'math'
require 'nn'	
require 'nngraph'
require 'optim'

local TemporalIdentityModel = torch.class('sportsbet.TemporalIdentityModel', 'sportsbet.TemporalModel')


function TemporalIdentityModel:__init(config, trainData)
	sportsbet.TemporalModel.__init(self, config)
	self.config.model = 'TemporalIdentityModel'


	self.LKTIndex, self.LKTIndexSize = initLKTIndex(trainData)
	self.trainData, self.TLKTIndex, self.TLKTIndexSize = initTLKTIndex(trainData)

	-- model Module
	self.mInput = nn.Identity()()
	self.mTLKT = nn.LookupTable(self.TLKTIndexSize, self.config.jSize)(self.mInput)
	self.mSplit = nn.SplitTable(2)(self.mTLKT)
	self.mJ1 = nn.SelectTable(1)(self.mSplit)
	self.mJ2 = nn.SelectTable(2)(self.mSplit)
	self.mLin1 = nn.Linear(self.config.jSize, self.config.jSize)(self.mJ1)
	self.mLin2 = nn.Linear(self.config.jSize, self.config.jSize)(self.mJ2)
	self.mAdd = nn.CAddTable()({self.mLin1, self.mLin2})
	self.mTanh = nn.Tanh()(self.mAdd)
	self.mLin = nn.Linear(self.config.jSize, self.config.nClasses)(self.mTanh)
	self.mSM = nn.SoftMax()(self.mLin)
	self.model = nn.gModule({self.mInput}, {self.mSM})

	--train Module
	self.tInput = nn.Identity()()
	self.tModInput = nn.SelectTable(1)(self.tInput)
	self.tZM = nn.SelectTable(2)(self.tInput)
	self.tModel = self.model(self.tModInput)
	self.trainModule = nn.gModule({self.tInput}, {self.tModel, self.tZM})

	-- parallel Criterion
	self.parCrit = nn.ParallelCriterion()
	self.parCrit:add(nn.CrossEntropyCriterion())
	self.parCrit:add(nn.MSECriterion(), self.config.lambda)

	-- initialisation
	self.trainModule:reset(self.config.initValues)

	parameters, gradParameters = self.trainModule:getParameters()
end

