local math = require 'math'
local table = require 'table'



local RankingModel = torch.class('sportsbet.RankingModel','sportsbet.AbstractModel')



function RankingModel:__init(config)
	self.config = config
end 



-- Creates a ranking table: key player_id, value: number of wins in data
function RankingModel:fitStep(data)

	self.ranking = {}

	for i =1,#data do
		self.ranking[data[i]['j1_id']] = 0
		self.ranking[data[i]['j2_id']] = 0
	end
	for i =1,#data do
		if data[i]['winner'] == -1 then
			self.ranking[data[i]['j1_id']] = self.ranking[data[i]['j1_id']]+1
		elseif data[i]['winner'] == 1 then
			self.ranking[data[i]['j2_id']] = self.ranking[data[i]['j2_id']]+1
		end
	end
end


-- Runs through xdata, and for each match of xdata, predicts
-- the corresponding label based on the max number of wins
-- (computed in fitStep).
function RankingModel:predict(xdata)

	local labels = {}
	local nilvalues = 0

	for i = 1,#xdata do

		local j1_id = xdata[i]['j1_id']
		local j2_id = xdata[i]['j2_id']
		local j1_wins = self.ranking[j1_id]
		local j2_wins = self.ranking[j2_id]

		if j1_wins == nil and j2_wins == nil then
			nilvalues = nilvalues+1
		end

		if j1_wins and j2_wins then

			if j2_wins > j1_wins then
				labels[i] = 1
			else
				labels[i] = -1
			end

		else
			
			labels[i] = 'nil'

		end
	
	end

	return labels
end
