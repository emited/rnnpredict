require 'math'
require 'nn'	
require 'nngraph'
require 'optim'

nngraph.setDebug(true)

local FeaturesSubModel = torch.class('sportsbet.FeaturesSubModel', 'sportsbet.AbstractModel')


function FeaturesSubModel:__init(config, trainData)
	-- Warning feature country isn't surjective
	self.config = {
		jSize = config.jSize or 15,
		nClasses = config.nClasses or 2,
		learningRate = config.learningRate or 0.01,
		learningRateDecay = config.learningRateDecay or 1e-4,
		weightDecay = config.weightDecay or 0,
		momentum = config.momentum or 0,
		batchSize = config.batchSize or 15,
		initValues = initValues or 0.1,
		alg = config.alg or 'sgd',
		features = config.features or {surface=true, level=true, age=true, country=true, round=true}
	}

	if tableGetLength(self.config.features) ~= 0 then
		self:initFeaturesDim(trainData)
	end
	
	self.LKTIndex, self.LKTIndexSize = initLKTIndex(trainData)
	print('self.LKTIndexSize: '..self.LKTIndexSize)
	
	self.input1 = nn.Identity()()
	self.lktPlayer = nn.LookupTable(self.LKTIndexSize, self.config.jSize)(self.input1)
	self.splitPlayer = nn.SplitTable(2)(self.lktPlayer)
	self.selectJ1 = nn.SelectTable(1)(self.splitPlayer)
	self.selectJ2 = nn.SelectTable(2)(self.splitPlayer)
	-- self.linJ1 = nn.Linear(self.config.jSize, self.config.jSize)(self.selectJ1)
	-- self.linJ2 = nn.Linear(self.config.jSize, self.config.jSize)(self.selectJ2)
	self.subPlayers = nn.CSubTable()({self.selectJ1, self.selectJ2})
	self.linPlayers = nn.Linear(self.config.jSize, self.config.jSize)(self.subPlayers)

	self.input2 = nn.Identity()()
	self.input3 = nn.Identity()()
	self.lktContexts = nn.LookupTable(self.LKTIndexSize, self.featuresDim)(self.input1)
	self.splitContexts = nn.SplitTable(2)(self.lktContexts)
	self.selectContextJ1 = nn.SelectTable(1)(self.splitContexts)
	self.contJ1 = nn.Contiguous()(self.selectContextJ1)
	self.viewContextJ1 = nn.View(-1, self.featuresDim, 1)(self.contJ1)
	self.selectContextJ2 = nn.SelectTable(2)(self.splitContexts)
	self.contJ2 = nn.Contiguous()(self.selectContextJ2)
	self.viewContextJ2 = nn.View(-1, self.featuresDim, 1)(self.contJ2)
	-- self.linContextJ1 = nn.Linear(self.featuresNumber, self.featuresNumber)(self.selectContextJ1)
	-- self.linContextJ2 = nn.Linear(self.featuresNumber, self.featuresNumber)(self.selectContextJ2)
	self.PContextJ1 = nn.MM(false, false)({self.input2, self.viewContextJ1})
	self.PContextJ2 = nn.MM(false, false)({self.input3, self.viewContextJ2})
	self.addContexts = nn.CAddTable()({self.PContextJ1, self.PContextJ2})
	self.viewContexts = nn.View(-1, self.featuresNumber)(self.addContexts)

	self.linContexts = nn.Linear(self.featuresNumber, self.config.jSize)(self.viewContexts)
	self.add = nn.CAddTable()({self.linPlayers, self.linContexts})
	self.tanh = nn.Tanh()(self.add)
	self.lin = nn.Linear(config.jSize, self.config.nClasses)(self.tanh)
	self.softmax = nn.SoftMax()(self.lin)
	self.model = nn.gModule({self.input1, self.input2, self.input3}, {self.softmax})
	self.criterion = nn.CrossEntropyCriterion()

	self.model:reset(self.config.initValues)

	parameters, gradParameters = self.model:getParameters()
end

function FeaturesSubModel:buildContextMatrix(match, debug)

	if self.featuresDim == 0 then return nil end

	dateN = 1000000000
	dim1 = self.featuresNumber
	dim2 = self.featuresDim
	if debug then print(self.countries) end
	if debug then print(match) end
	if debug then print("dimensions", dim1, dim2) end
	w_context_j1 = torch.Tensor(dim1, dim2):zero()
	w_context_j2 = torch.Tensor(dim1, dim2):zero()

	for w_context in sportsbet.Tools:list_iter({{[0] = w_context_j1, [1] = 'j2'}, {[0] = w_context_j2, [1] = 'j1'}}) do
		k = 0
		features = sportsbet.Tools:shallow_copy(self.config.features)
		if debug then print(features) end
		for i=1, dim1 do
			if features["surface"] then
				w_context[0][i][k+match["surface"]] = 1
				features["surface"] = nil
				if debug then print(i,k+match["surface"],match["surface"]) end
				k = k + 6
			elseif features["level"] then
				w_context[0][i][k+match["level"]] = 1
				features["level"] = nil
				if debug then print(i,k+match["level"],match["level"]) end
				k = k + 6
			elseif features["age"] then
				w_context[0][i][k+1] = (dateToNumber(match[w_context[1].."_age"]))/dateN
				features["age"] = nil
				if debug then print(i, k+1,dateToNumber(match[w_context[1].."_age"])/dateN) end
				k = k + 1
			elseif features["country"] then
				w_context[0][i][k + self.countries[match[w_context[1].."_country"]]] = 1
				if debug then print(i, k + self.countries[match[w_context[1].."_country"]],self.countries[match["j1_country"]]) end
				k = k + self.countriesDim
				features["country"] = nil
			elseif features["round"] then
				w_context[0][i][k + 1] = match["round"]
				if debug then print(i, k + match["round"]) end
				k = k + 1
				features["round"] = nil
			end
		end
	end
	if debug then print(w_context_j1, w_context_j2) end
	return w_context_j1, w_context_j2
end
		
		
function initLKTIndex(data)

	index = {}
	local iter = 1

	for i = 1,#data do
		j1_id = data[i]['j1_id']
		j2_id = data[i]['j2_id']
		if index[j1_id] == nil then
			index[j1_id] = iter
			iter = iter+1
		end
		if index[j2_id] == nil then
			index[j2_id] = iter
			iter = iter+1
		end
	end
	return index, iter
end



function FeaturesSubModel:fitStep(data)
	
	local batchSize = self.config.batchSize
	local featuresDim = self.config.featuresDim
	local features = self.config.features

	if batchSize > #data then
		error('batchSize > data.')
	end

	local idxmatch = torch.randperm(#data)
	local err = 0

	for cursor = 1, #data, batchSize do

		-- initializing batch
		local_size = math.min(batchSize, #data - cursor  + 1)
		if local_size == batchSize then
			local input1 = torch.Tensor(local_size, 2)
			local contextJ1 = torch.Tensor(local_size, self.featuresNumber, self.featuresDim)
			local contextJ2 = torch.Tensor(local_size, self.featuresNumber, self.featuresDim)
			local targets = torch.Tensor(local_size)

			-- filling the batch
			local k = 1
			for i = cursor, math.min(cursor + batchSize -1, #data) do

				local match = data[idxmatch[i]]
				input1[k][1] = self.LKTIndex[match['j1_id']]
				input1[k][2] = self.LKTIndex[match['j2_id']]

				contextJ1[k], contextJ2[k] = self:buildContextMatrix(match, false)
				if self.config.nClasses == 2 then
					local idx = {[-1] = 1, [1] = 2}
					targets[k] = idx[match['winner']]
				elseif self.config.nClasses == 3 then 
					local idx = {[-1] = 1, [0] = 2, [1] = 3}
					targets[k] = idx[match['winner']]
				end

				k = k + 1
			end

			input = {}
			input[1] = input1
			input[2] = contextJ1
			input[3] = contextJ2
			--[[ debug mode self.model.name = 'model'
			pcall(function() self.model:updateOutput(input) end) ]]--

			-- evaluating loss and gradient of loss
			local feval = function(x)
				--collectgarbage()

				if x ~= parameters then
					parameters:copy(x)
				end

				gradParameters:zero()

				local preds = self.model:forward(input)
				local f = self.criterion:forward(preds, targets)

				local df_do = self.criterion:backward(preds, targets)
				self.model:backward(input, df_do)
				return f, gradParameters
			end


			-- sgd parameters
			alg_params = {
					learningRate = self.config.learningRate * self.config.batchSize,
					learningRateDecay = self.config.learningRateDecay * self.config.batchSize,
					weightDecay = self.config.weightDecay,
					momentum = self.config.momentum
			}

			_, fs = optim.sgd(feval, parameters, alg_params)
			err = err + fs[1]
		end
	end
	return err/math.floor(#data/batchSize)
end




function FeaturesSubModel:predict(data)
	local k = 1
	local matchIdx = {}
	local invMatchIdx = {}

	--selecting matches to predict, if both players are in LKTIndex.
	for i =1, #data do
		local match = data[i]
		if self.LKTIndex[match['j1_id']] and self.LKTIndex[match['j2_id']] then
			matchIdx[k] = i
			invMatchIdx[i] = k
			k = k + 1
		end
	end
	
	-- putting matches in a torch.Tensor, if they are any.
	if k > 1 then

		local j1j2 = torch.Tensor(k-1, 2)
		local contextJ1 = torch.Tensor(k-1, self.featuresNumber, self.featuresDim)
		local contextJ2 = torch.Tensor(k-1, self.featuresNumber, self.featuresDim)
		local winners = torch.Tensor(k-1)
		for i = 1, k-1 do
			local match = data[matchIdx[i]]
			j1j2[i][1] = self.LKTIndex[match['j1_id']]
			j1j2[i][2] = self.LKTIndex[match['j2_id']]
			contextJ1[i], contextJ2[i] = self:buildContextMatrix(match, false)
			winners[i] = match['winner']
		end

		input = {}
		input[1] = j1j2
		input[2] = contextJ1
		input[3] = contextJ2

		-- guetting model results
		local probsTensor = self.model:forward(input, winners)

		-- extracting max and argmax
		maxprob, maxindex = torch.max(probsTensor,2)

	end

	if self.config.nClasses == 2 then
		invIndex = {-1, 1}
	elseif self.config.nClasses == 3 then
		invIndex = {-1, 0, 1}
	end

	-- filling tables of preds and probs
	local preds, probs = {}, {}
	for i = 1, #data do
		if invMatchIdx[i] then
			preds[i] = invIndex[maxindex[invMatchIdx[i]][1]]
			probs[i] = maxprob[invMatchIdx[i]][1]
		else
			preds[i] = 'nil'
			probs[i] = 'nil'
		end
	end
	return preds, probs
end

function FeaturesSubModel:initFeaturesDim(data)

	d = 0
	n = 0
	features = self.config.features

	if features['surface'] then
		d = d + 6
		n = n + 1
	end
	if features['level'] then
		d = d + 6
		n = n + 1
	end
	if features['country'] then
		id = 1
		countries = {}
		for i=1, #data do
			match = data[i]
			if match.j1_country
				and countries[match.j1_country] == nil then
					countries[match.j1_country] = id
					id = id + 1
			end
			if match.j2_country
				and countries[match.j2_country] == nil then
					countries[match.j2_country] = id
					id = id + 1
			end
		end
		id = id - 1
		self.countries = countries
		self.countriesDim = id
		d = d + id
		n = n + 1
	end
	if features['age'] then
		d = d + 1
		n = n + 1
	end
	if features['round'] then
		d = d + 1
		n = n + 1
	end
	self.featuresDim = d
	self.featuresNumber = n
end
