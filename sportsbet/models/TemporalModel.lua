require 'math'
require 'nn'	
require 'nngraph'
require 'optim'

local TemporalModel = torch.class('sportsbet.TemporalModel', 'sportsbet.AbstractModel')


function TemporalModel:__init(config, trainData)
	self.config = {
		jSize = config.jSize or 15,
		nClasses = config.nClasses or 2,
		learningRate = config.learningRate or 0.01,
		learningRateDecay = config.learningRateDecay or 1e-4,
		weightDecay = config.weightDecay or 0,
		momentum = config.momentum or 0,
		batchSize = config.batchSize or 15,
		initValues = config.initValues or 0.1,
		lambda = config.lambda or 0.4,
		alg = config.alg or 'sgd'
	}

end



function initLKTIndex(data)

	index = {}
	local iter = 1

	for i = 1, #data do
		j1_id = data[i]['j1_id']
		j2_id = data[i]['j2_id']
		if index[j1_id] == nil then
			index[j1_id] = iter
			iter = iter + 1
		end
		if index[j2_id] == nil then
			index[j2_id] = iter
			iter = iter + 1
		end
	end
	return index, iter
end


function initTLKTIndex(data)

	local idx = 1
	local current, TLKTIndex = {}, {}
	local newData = sportsbet.Tools:shallow_copy(data)

	for i = 1, #newData do
		local match = newData[i]
		local j1 = {[0] = match.j1_id, [1] = 'j1_t'}
		local j2 = {[0] = match.j2_id, [1] = 'j2_t'}
		for j in sportsbet.Tools:list_iter({j1, j2}) do
			if j[0] then
				if current[j[0]] == nil then
					current[j[0]] = 0
					TLKTIndex[j[0]] = {}
				end
			current[j[0]] = current[j[0]] + 1
			match[j[1]] = current[j[0]]
			table.insert(TLKTIndex[j[0]], idx)
			idx = idx + 1
			end
		end
	end
	return newData, TLKTIndex, idx - 1
end






function TemporalModel:nextBatch()

-- returns tensors of player ids with associated
-- winner's location of size self.config.batchSize.
-- These tensors do not contain matches where a 
-- player played for the first time in self.trainData.

	local batchSize = self.config.batchSize
	self.perm = self.perm or torch.randperm(#self.trainData)
	self.cursor = self.cursor or 1

	local j_id, jm_id, labels = {}, {}, {}
	local k, idx = 0, 1

	while idx <= batchSize and self.cursor + k <= #self.trainData do
		
		local match = self.trainData[self.perm[self.cursor + k]]

		if match.j1_t ~= 1 and match.j2_t ~= 1 then

			j_id[idx] = {}
			j_id[idx][1] = self.TLKTIndex[match.j1_id][match.j1_t]
			j_id[idx][2] = self.TLKTIndex[match.j2_id][match.j2_t]
			
			jm_id[idx] = {}
			jm_id[idx][1] = self.LKTIndex[match.j1_id]
			jm_id[idx][2] = self.LKTIndex[match.j2_id]

			if self.config.nClasses == 2 then
				local winLoc = {[-1] = 1, [1] = 2}
				labels[idx] = winLoc[match.winner]
			elseif self.config.nClasses == 3 then 
				local winLoc = {[-1] = 1, [0] = 2, [1] = 3}
				labels[idx] = winLoc[match.winner]
			end

			idx = idx + 1

		end

		k = k + 1

	end

	self.cursor = self.cursor + k
	return torch.Tensor(j_id),torch.Tensor(jm_id), torch.Tensor(labels)
end



function TemporalModel:savePlayers(path, filename)

	local logP = sportsbet.ExperimentLogCSV(true, path, filename)

	for i, j_id in pairs(self.TLKTIndex) do

		local z = self.model:get(2):forward(torch.Tensor(j_id)):clone()

		for nmatch = 1, z:size(1) do
			local z_t = {}

			for nz = 1, z:size(2) do
				z_t[nz] = z[nmatch][nz]
			end

			logP:newIteration()
			logP:addValue('player', i)
			logP:addValue('match', nmatch)
			logP:addValue('z', z_t)

		end

	end

end



function TemporalModel:fitStep()
	
	local err = 0
	self.cursor = 1

	while self.cursor < #self.trainData do

		--get ids and labels from next batch
		local j_id, jm_id , labels = self:nextBatch()


		-- computes the gradient and parameters
		local feval = function(x)
			
			if x ~= parameters then
				parameters:copy(x)
			end
			
			gradParameters:zero()

			local zm = self.model:get(2):forward(jm_id):clone()
			local z = self.model:get(2):forward(j_id):clone()
			local tOut = self.trainModule:forward({j_id, zm})
			local loss_x = self.parCrit:forward(tOut, {labels, z})
			local dloss_x = self.parCrit:backward(tOut, {labels, z})
			self.trainModule:backward({j_id, zm}, dloss_x)

			return loss_x, gradParameters
		end


		alg_params = {
					learningRate = self.config.learningRate * self.config.batchSize,
					learningRateDecay = self.config.learningRateDecay * self.config.batchSize,
					weightDecay = self.config.weightDecay,
					momentum = self.config.momentum
		}

		if self.config.alg == 'sgd' then
			_, fs = optim.sgd(feval, parameters, alg_params)
		elseif self.config.alg == 'adagrad' then
			_, fs = optim.adagrad(feval, parameters, alg_params)
		elseif self.config.alg == 'adam' then
			_, fs = optim.adam(feval, parameters, alg_params)
		elseif self.config.alg == 'nag' then
			_, fs = optim.nag(feval, parameters, alg_params)
		end

		err = err + fs[1]

	end

	return err/math.floor(#self.trainData/self.config.batchSize)  -- although last batchsize may vary
end






function TemporalModel:getPredMatches(data)
-- returns torch.Tensor of matches where both players
-- are in self.trainData, with their associated index
-- locations in the initial data table.

	local matchLoc = {}
	local j = {}
	local idx = 1
	
	for k = 1, #data do

		local match = data[k]
		local all_j1 = self.TLKTIndex[match.j1_id]
		local all_j2 = self.TLKTIndex[match.j2_id]
		
		if all_j1 and all_j2 then

			if #data == #self.trainData then -- naive way to test if training data

				if match.j1_t ~= 1 and match.j2_t ~= 1 then
					j[idx] = {}
					j[idx][1] = all_j1[match.j1_t]
					j[idx][2] = all_j2[match.j2_t]
					matchLoc[k] = idx
					idx = idx + 1
				end

			else
				j[idx] = {}
				j[idx][1] = all_j1[#all_j1]
				j[idx][2] = all_j2[#all_j2]
				matchLoc[k] = idx
				idx = idx + 1
			end

		end
	end

	return torch.Tensor(j), matchLoc
end



function TemporalModel:predict(data)
	
	--extraction of matches where players are in TLKTIndex	
	local j_id, matchLoc = self:getPredMatches(data)

	-- prediction
	local output = self.model:forward(torch.Tensor(j_id))
	local max, argmax = torch.max(output,2)

	if self.config.nClasses == 2 then
		invWinLoc = {-1, 1}
	elseif self.config.nClasses == 3 then
		invWinLoc = {-1, 0, 1}
	end

	-- filling tables of preds and probs
	local preds, probs = {}, {}

	for i = 1, #data do

		if matchLoc[i] then
			preds[i] = invWinLoc[argmax[matchLoc[i]][1]]
			probs[i] = max[matchLoc[i]][1]
		else
			preds[i] = 'nil'
			probs[i] = 'nil'
		end

	end
	return preds, probs	
end
