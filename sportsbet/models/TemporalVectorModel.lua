
local TemporalVectorModel = torch.class('sportsbet.TemporalVectorModel', 'sportsbet.TemporalModel')



function TemporalVectorModel:__init(config, trainData)
	sportsbet.TemporalModel.__init(self, config)
	self.config.model = 'TemporalVectorModel'


	self.LKTIndex, self.LKTIndexSize = initLKTIndex(trainData)
	self.trainData, self.TLKTIndex, self.TLKTIndexSize = initTLKTIndex(trainData)

	-- model Module
	self.mInput = nn.Identity()()
	self.mTLKT = nn.LookupTable(self.TLKTIndexSize, self.config.jSize)(self.mInput)
	self.mSplit = nn.SplitTable(2)(self.mTLKT)
	self.mJ1 = nn.SelectTable(1)(self.mSplit)
	self.mJ2 = nn.SelectTable(2)(self.mSplit)
	self.mLin1 = nn.Linear(self.config.jSize, self.config.jSize)(self.mJ1)
	self.mLin2 = nn.Linear(self.config.jSize, self.config.jSize)(self.mJ2)
	self.mAdd = nn.CAddTable()({self.mLin1, self.mLin2})
	self.mTanh = nn.Tanh()(self.mAdd)
	self.mLin = nn.Linear(self.config.jSize, self.config.nClasses)(self.mTanh)
	self.mSM = nn.SoftMax()(self.mLin)
	self.model = nn.gModule({self.mInput}, {self.mSM})

	--train Module
	self.tInput = nn.Identity()()
	self.tModInput = nn.SelectTable(1)(self.tInput)
	self.tJM = nn.SelectTable(2)(self.tInput)
	self.tZM = nn.SelectTable(3)(self.tInput)
	self.tModel = self.model(self.tModInput)
	self.tTLKT = nn.LookupTable(self.LKTIndexSize, self.config.jSize)(self.tJM)
	self.tZMTheta = nn.CMulTable()({self.tTLKT, self.tZM})
	self.tZMThetaP1 = nn.CAddTable()({self.tZMTheta, self.tZM})
	self.trainModule = nn.gModule({self.tInput}, {self.tModel, self.tZMThetaP1})

	-- parallel Criterion
	self.parCrit = nn.ParallelCriterion()
	self.parCrit:add(nn.CrossEntropyCriterion())
	self.parCrit:add(nn.MSECriterion(), self.config.lambda)

	-- initialisation
	self.trainModule:reset(self.config.initValues)

	parameters, gradParameters = self.trainModule:getParameters()
end


function TemporalVectorModel:fitStep()
	
	local err = 0
	self.cursor = 1

	while self.cursor < #self.trainData do

		--get ids and labels from next batch
		local j_id, jm_id , labels = self:nextBatch()


		-- computes the gradient and parameters
		local feval = function(x)
			
			if x ~= parameters then
				parameters:copy(x)
			end
			
			gradParameters:zero()

			local zm = self.model:get(2):forward(jm_id):clone()
			local z = self.model:get(2):forward(j_id):clone()
			local tOut = self.trainModule:forward({j_id, jm_id, zm})
			local loss_x = self.parCrit:forward(tOut, {labels, z})
			local dloss_x = self.parCrit:backward(tOut, {labels, z})
			self.trainModule:backward({j_id, jm_id, zm}, dloss_x)

			return loss_x, gradParameters
		end


		alg_params = {
					learningRate = self.config.learningRate * self.config.batchSize,
					learningRateDecay = self.config.learningRateDecay * self.config.batchSize,
					weightDecay = self.config.weightDecay,
					momentum = self.config.momentum
		}

		if self.config.alg == 'sgd' then
			_, fs = optim.sgd(feval, parameters, alg_params)
		elseif self.config.alg == 'adagrad' then
			_, fs = optim.adagrad(feval, parameters, alg_params)
		elseif self.config.alg == 'adam' then
			_, fs = optim.adam(feval, parameters, alg_params)
		end

		err = err + fs[1]

	end

	return err/math.floor(#self.trainData/self.config.batchSize)  -- although last batchsize may vary
end
