require 'math'
require 'nn'	
require 'nngraph'
require 'optim'

local MatrixFactBiModel = torch.class('sportsbet.MatrixFactBiModel', 'sportsbet.MatrixFactModel')


function MatrixFactBiModel:__init(config, trainData)
	
	sportsbet.MatrixFactModel.__init(self, config)
	self.config.model = 'MatrixFactBiModel'

	self.LKTIndex, self.LKTIndexSize = initLKTIndex(trainData)
	--print('self.LKTIndexSize: '..self.LKTIndexSize)
	
	self.input = nn.Identity()()
	self.lkt = nn.LookupTable(self.LKTIndexSize,self.config.jSize)(self.input)
	self.split = nn.SplitTable(2)(self.lkt)
	self.select1 = nn.SelectTable(1)(self.split)
	self.select2 = nn.SelectTable(2)(self.split)
	self.lin1 = nn.Linear(self.config.jSize, self.config.jSize)(self.select1)
	self.lin2 = nn.Linear(self.config.jSize, self.config.jSize)(self.select2)
	self.add = nn.CAddTable()({self.lin1, self.lin2})
	self.tanh = nn.Tanh()(self.add)
	self.lin = nn.Linear(config.jSize, self.config.nClasses)(self.tanh)
	self.softmax = nn.SoftMax()(self.lin)
	self.model = nn.gModule({self.input}, {self.softmax})
	self.criterion = nn.CrossEntropyCriterion()

	self.model:reset(self.config.initValues)

	parameters, gradParameters = self.model:getParameters()
end