require 'nn'
require 'nngraph'
require 'math'

local ScalarModel = torch.class('sportsbet.ScalarModel','sportsbet.AbstractModel')

-- Takes the trainnig data in order to create LookupTable
-- with jSize the size of the wector representation of the player
function ScalarModel:__init(trainData,jSize)

	params = {}
	params['jSize'] = jSize

	self.LKTIndex, self. LKTIndexSize = initLKTIndex(trainData)
	sportsbet.AbstractModel.__init(self,params)
	self.input = nn.Identity()()
	self.lkt = nn.LookupTable(self.LKTIndexSize,self.params.jSize)(self.input)
	self.split = nn.SplitTable(1)(self.lkt)
	self.select1 = nn.SelectTable(1)(self.split)
	self.select2 = nn.SelectTable(2)(self.split)
	self.dot = nn.DotProduct()({self.select1,self.select2})
	self.model = nn.gModule({self.input},{self.dot})
	self.criterion = nn.MSECriterion()
	--print(self.lkt.data.module.weight)
end 

function initLKTIndex(data)

	index = {}
	local iter = 1

	for i = 1,#data do
		j1_id = data[i]['j1_id']
		j2_id = data[i]['j2_id']
		if( type(index[j1_id]) == 'nil') then
			index[j1_id] = iter
			iter = iter+1
		end
		if( type(index[j2_id]) == 'nil') then
			index[j2_id] = iter
			iter = iter+1
		end
	end
	return index, iter
end

function ScalarModel:fitStep(data, eps, innersteps)
	if( not eps) then
		eps = 0.01
	end
	if( not innersteps) then
		innersteps = 1
	end
	for i = 1, #data do
		local match = data[i]
		local j = torch.Tensor(2)
		j[1] = self.LKTIndex[match['j1_id']]
		j[2] = self.LKTIndex[match['j2_id']]
		local y = torch.Tensor(1)
		y[1] = match['winner']
		for step = 1,innersteps do 
			self.model:zeroGradParameters()
			local pred = self.model:forward(j)
			local err = self.criterion:forward(pred,y)
			local delta = self.criterion:backward(pred,y)
			self.model:backward(j,delta)
			self.model:updateParameters(0.01)
		end
	end
end

function ScalarModel:predict(data)

	local preds = {}

	for i = 1, #data do
		local match = data[i]
		local j = torch.Tensor(2)
		j1 = match['j1_id']
		j2 = match['j2_id']
		local pred = torch.Tensor(1)
		if( type(self.LKTIndex[j1]) ~= 'nil' 
			and type(self.LKTIndex[j2]) ~= 'nil' ) then
			j[1] = self.LKTIndex[j1]
			j[2] = self.LKTIndex[j2]
			pred[1] = self.model:forward(j)
			--[[print('match: '..i)
			print('j1')
			print(j[1])
			print('j2')
			print(j[2])
			print(pred[1])
			print('winner')
			print(match['winner'])
			print('')]]
			if(pred[1] <= 0) then
				preds[i] = -1
			else
				preds[i] = 1
			end
		else --players not in data
			--pred[1] = math.floor(pred[1]+0.5)*2-1
			preds[i] = 'nil'
		end
	end
	return preds
end
