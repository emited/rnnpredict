opt={}
opt.Tmin={"1990-01-01","1991-01-01","1992-01-01","1993-01-01","1994-01-01","1995-01-01","1996-01-01","1997-01-01","1998-01-01","1999-01-01","2000-01-01","2001-01-01","2002-01-01","2003-01-01","2004-01-01","2004-01-01","2005-01-01","2006-01-01","2007-01-01","2008-01-01","2009-01-01","2010-01-01","2011-01-01","2012-01-01","2013-01-01","2014-01-01","2015-01-01"}
opt.Tmax={"2060-01-01"}
opt.tStart={"1990-01-01"}
opt.nTrain={1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000}
opt.nTest={600}
opt.nValidation={200}
opt.shuffle={"true"}
opt.threshold={0}
opt.logPath={"./results/foot"}
opt.verbose={"false"}
opt.db={"db/foot_database"}

model_name="FootBetModel"
model_parameters={
  maxEpoch={300},
  stepEpoch={5},
  jSize = {1, 5, 10, 20, 30, 50},
  nClasses = {3},
  batchSize={5},
  learningRateDecay={0},
  learningRate={1e-3},
  weightDecay={0},
  momentum={0},
  initValues={0.05},
  lambda={6.5,16,5},
  alg='adagrad',
  module_name={"TemporalIdentityModel","TemporalLinearModel","TemporalVector"}
  }

executable="exp2.lua"
use_oarstat=false
max_nb_processes=200
debug=false
