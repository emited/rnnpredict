require 'lfs' --luarocks install luafilesystem
require 'lsqlite3'
require 'csvigo'

-- Ouverture de la base
local db = sqlite3.open("../wta_database")
assert(db:execute ("BEGIN TRANSACTION;"))
for current in io.lines("../Sources/tours_wta.csv") do
	line = {}
        for w in current:gmatch("[^;]*;") do
		w = w:sub(0, #w-1)
                table.insert(line, w)
        end
	new = {}
	date = line[4]
	for w in date:gmatch("[^/]*") do
		table.insert(new, w)
	end
	if (new[5]) then new[5] = new[5]:sub(1, 4) end
	if (new[5] and new[3] and new[1]) then
		new = new[5] .. "-" .. new[3] .. "-" .. new[1]
	end
        sql = string.format([[
                UPDATE matches
                SET date = "%s"
                WHERE date = "%s";
                ]], new, date)
	assert(db:execute(sql))
end
assert(db:execute ("COMMIT;"))
