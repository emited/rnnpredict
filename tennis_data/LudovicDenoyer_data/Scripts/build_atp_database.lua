require 'lfs' --luarocks install luafilesystem
require 'lsqlite3'
require 'lfs'
require 'csvigo'

path = '../Sources/games_atp.txt'

-- Creation de la base
local db = sqlite3.open("../atp_database")
local file = io.open(path)

assert(db:exec[[CREATE TABLE matches(tourney_id, tourney_name, surface, level, date, j1_id, j1_name, j1_age, j1_country, j2_id, j2_name, j2_age, j2_country, winner, score, round, j1_odds, j2_odds, PRIMARY KEY(tourney_id, j1_id, j2_id))]])

assert(db:execute ("BEGIN TRANSACTION;"))

local matches = csvigo.load({path = "../Sources/games_atp.csv", mode="large"})
for i=2, #matches do
	line = {}
	for w in matches[i][1]:gmatch("[^;]*;") do
		w = w:sub(0, #w-1)
		table.insert(line, w)
	end
	j1 = line[1]
	j2 = line[2]
	tournoi = line[3]
	round = line[4]
	score = line[5]
	sql = string.format([[
		INSERT INTO matches (j1_id, j2_id, tourney_id, round, score)
		VALUES ("%s", "%s", "%s", "%s", %s);
		]], j1, j2, tournoi, round, score)
	assert (db:execute(sql))
end

local players = csvigo.load({path = "../Sources/players_atp.csv", mode="large"})
for i=2, #players do
	line = {}
	for w in players[i][1]:gmatch("[^;]*;") do
		w = w:sub(0, #w-1)
		table.insert(line, w)
	end
	j1_id = line[1]
	j1_name = line[2]
	j1_age = line[3]
	j1_country = line[4]
	sql = string.format([[
		UPDATE matches
		SET j1_name = %s, j1_age = "%s", j1_country = %s
		WHERE j1_id = "%s";
		]], j1_name, j1_age, j1_country, j1_id)
	assert(db:execute(sql))
	sql = string.format([[
		UPDATE matches
		SET j2_name = %s, j2_age = "%s", j2_country = %s
		WHERE j2_id = "%s";
		]], j1_name, j1_age, j1_country, j1_id)
	assert(db:execute(sql))
end

for current in io.lines("../Sources/odds_atp.csv") do
	line = {}
        for w in current:gmatch("[^;]*;") do
		w = w:sub(0, #w-1)
                table.insert(line, w)
        end
        j1_id = line[2]
        j2_id = line[3]
        tourney_id = line[4]
        j1_odds = line[6]
        j2_odds = line[7]
        sql = string.format([[
                UPDATE matches
                SET j1_odds = "%s", j2_odds = "%s"
                WHERE j1_id = "%s" and j2_id = "%s" and tourney_id = "%s";
                ]], j1_odds, j2_odds, j1_id, j2_id, tourney_id)
	assert(db:execute(sql))
end

for current in io.lines("../Sources/tours_atp.csv") do
	line = {}
        for w in current:gmatch("[^;]*;") do
		w = w:sub(0, #w-1)
                table.insert(line, w)
        end
        tourney_id = line[1]
        tourney_name = line[2]
        terrain = line[3]
        date = line[4]
        level = line[5]
        sql = string.format([[
                UPDATE matches
                SET tourney_name = %s, surface = "%s", date = "%s", level = "%s"
                WHERE tourney_id = "%s";
                ]], tourney_name, terrain, date, level, tourney_id)
	assert(db:execute(sql))
end
assert(db:execute ("COMMIT;"))
