require 'lfs' --luarocks install luafilesystem
require 'lsqlite3'
require 'csvigo'

-- Ouverture de la base
local db = sqlite3.open("../wta_database")
assert(db:execute ("BEGIN TRANSACTION;"))
for a in db:nrows('SELECT * FROM matches p;') do 
	if(a['j1_odds'] ~= nil and a['j2_odds'] ~= nil) then
		j1_odds = string.gsub(a['j1_odds'], ',', '.')
		j2_odds = string.gsub(a['j2_odds'], ',', '.')
		sql = string.format([[
			UPDATE matches
			SET j1_odds = "%s", j2_odds = "%s"
			WHERE tourney_id = "%s" and j1_id = "%s" and j2_id = "%s";
			]], j1_odds, j2_odds, a['tourney_id'], a['j1_id'], a['j2_id'])
		assert(db:execute(sql))
	end
end
assert(db:execute ("COMMIT;"))
