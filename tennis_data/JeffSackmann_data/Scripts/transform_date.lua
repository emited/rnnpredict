require 'lfs' --luarocks install luafilesystem
require 'lsqlite3'
require 'csvigo'

-- Ouverture de la base
local db = sqlite3.open("../atp_database")
assert(db:execute ("BEGIN TRANSACTION;"))
for file in lfs.dir[[../Sources/tennis_atp/]] do
	if string.sub(file, 1, string.len("atp_matches_")) == "atp_matches_" then
		-- Ouverture des fichier matches
		local matches = csvigo.load({path = "../Sources/tennis_atp/" .. file, mode="large"})
		for i=2, #matches do
			date = matches[i][6]
			date = (string.sub(date, 1, 4)) .. "-" .. string.sub(date, 5, 6) .. "-" .. string.sub(date, 7, 8)
			sql = string.format([[
				UPDATE matches
				SET date = "%s"
				WHERE tourney_id = "%s" and num = "%s";]],
					date, matches[i][1], matches[i][7])
			--print(sql)
			assert (db:execute(sql))
		end
	end
end
assert(db:execute ("COMMIT;"))
local db = sqlite3.open("../wta_database")
assert(db:execute ("BEGIN TRANSACTION;"))
for file in lfs.dir[[../Sources/tennis_wta/]] do
	if string.sub(file, 1, string.len("wta_matches_")) == "wta_matches_" then
		-- Ouverture des fichier matches
		local matches = csvigo.load({path = "../Sources/tennis_wta/" .. file, mode="large"})
		for i=2, #matches do
			date = matches[i][6]
			date = (string.sub(date, 1, 4)) .. "-" .. string.sub(date, 5, 6) .. "-" .. string.sub(date, 7, 8)
			sql = string.format([[
				UPDATE matches
				SET date = "%s"
				WHERE tourney_id = "%s" and num = "%s";]],
					date, matches[i][1], matches[i][7])
			--print(sql)
			assert (db:execute(sql))
		end
	end
end
assert(db:execute ("COMMIT;"))
