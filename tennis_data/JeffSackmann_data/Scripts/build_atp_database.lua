require 'lfs' --luarocks install luafilesystem
require 'lsqlite3'
require 'csvigo'

-- Creation de la base
local db = sqlite3.open("../atp_database")

db:exec[[CREATE TABLE matches(tourney_id, tourney_name,
	surface, draw_size, level, date, num, j1_id, j1_name, j1_hand, j1_age,
	j1_country, j1_ht, j1_rk, j1_rkpts, j2_id, j2_name, j2_age, j2_country,
	j2_ht, j2_rk, j2_rkpts, winner, score, best_of, round, minutes, j1_ace, j1_df,
	j1_svpt, j1_1stIn, j1_1stWon, j1_2ndWon, j1_SvGms, j1_bpSaved, j1_bpFaced,
	j2_ace, j2_df, j2_svpt, j2_1stIn, j2_1stWon, j2_2ndWon, j2_SvGms, j2_bpSaved,
	j2_bpFaced, PRIMARY KEY(tourney_id, num))]]

-- Expressions pour la creation de la table Matches

local insert_matches = db:prepare[[
	INSERT INTO matches VALUES (:tourney_id, :tourney_name, :surface,
	:draw_size, :level, :date, :num, :j1_id, :j1_name, :j1_hand, :j1_age,
	:j1_country, :j1_ht, :j1_rk, :j1_rkpts, :j2_id, :j2_name, :j2_age,
	:j2_country, :j2_ht, :j2_rk, :j2_rkpts, :winner, :score, :best_of, :round,
	:minutes, :j1_ace, :j1_df, :j1_svpt, :j1_1stIn, :j1_1stWon, :j1_2ndWon,
	:j1_SvGms, :j1_bpSaved, :j1_bpFaced, :j2_ace, :j2_df, :j2_svpt, :j2_1stIn,
	:j2_1stWon, :j2_2ndWon, :j2_SvGms, :j2_bpSaved, :j2_bpFaced); ]]


j = 0
for file in lfs.dir[[../Sources/tennis_atp/]] do
	if string.sub(file, 1, string.len("atp_matches_")) == "atp_matches_" then
		-- Ouverture des fichier matches
		local matches = csvigo.load({path = "../Sources/tennis_atp/" .. file, mode="large"})
		-- Creation de la table matches
		print("A partir du fichier : tennis_atp/" .. file)
		for i=2, #matches do
			j = j + 1
			insert_matches:bind_names{
				tourney_id = matches[i][1], tourney_name = matches[i][2], surface = matches[i][3], draw_size = matches[i][4],
				level = matches[i][5], date = matches[i][6], num = matches[i][7], j1_id = matches[i][8], j1_name = matches[i][11], j1_hand = matches[i][12],
				j1_age = matches[i][15], j1_country = matches[i][14], j1_ht = matches[i][13], j1_rk = matches[i][16], j1_rkpts = matches[i][17], j2_id = matches[i][18],
				j2_name = matches[i][21], j2_age = matches[i][25], j2_country = matches[i][24], j2_ht = matches[i][23], j2_rk = matches[i][26], j2_rkpts = matches[i][27],
				winner = '-1', score = matches[i][28], best_of = matches[i][29], round = matches[i][30], minutes = matches[i][31], j1_ace = matches[i][32],
				j1_df = matches[i][33], j1_svpt = matches[i][34], j1_1stIn = matches[i][35], j1_1stIn = matches[i][36], j1_1stWon = matches[i][37], j1_2ndWon = matches[i][38],
				j1_SvGms = matches[i][38], j1_bpSaved = matches[i][39], j1_bpFaced = matches[i][40], j2_ace = matches[i][41], j2_df = matches[i][42], j2_svpt = matches[i][43],
				j2_1stIn = matches[i][44], j2_1stWon = matches[i][45], j2_2ndWon = matches[i][46], j2_SvGms = matches[i][47], j2_bpSaved = matches[i][48], j2_bpFaced = matches[i][49] }
			insert_matches:step()
			insert_matches:reset()
		end
	end
end
