import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from textwrap import wrap


def get_df(log,x):

    '''input:
            log: pandas.DataFrame
            itf: index num of timeFrame to plot
    '''
    return log[log['timeFrame']==x]


def plot_epochs(log,x,x_title,columns,figsize=(10,6),error=True):

    '''input:
            log: pandas.DataFrame,
            x: abscissa to plot  
            x_title: abscissa title
    '''    
    ax = plt.gca()
    err = (' error' if error else ' score')
    for col in columns:
        if(error):
            df = get_df(log,x)[col].apply(lambda x:x if('loss' in col or 'Loss' in col and err) else 1-x)
            df.plot(x='epoch',y=col,title='',figsize=figsize,label=log['model'].loc[0]+': '+col+err,ax=ax)
        else:
            get_df(log,x).plot(x=x_title,y=col,title='',figsize=figsize,label=log['model'].loc[0]+': '+col+err,ax=ax)
    plt.xlabel('epochs')
    plt.ylabel(err)
    plt.ylim([0,1])
    plt.title('TimeFrame '+str(x))
    plt.legend(bbox_to_anchor=(1.55, .8), bbox_transform=ax.transAxes)
    
    #plt.legend()


def plot_dfs(log,epoch,columns):

    '''input:
            log: pandas.DataFrame,    
    '''
    ax=plt.gca()
    x_col='timeFrame'
    all_tfs=log[log['epoch']==epoch]
    for col in columns:
        all_tfs.plot(x=x_col,y=col,title='',label=log['model'].loc[0]+': '+col,ax=ax)
    plt.xlabel(x_col)
    plt.ylabel('score')
    plt.ylim([0,1])
    plt.title('epoch '+str(epoch))
    plt.legend(bbox_to_anchor=(1.55, .8), bbox_transform=ax.transAxes)


def plot_bank(log,loc):

    naiveBank = eval(log['naiveBank'].loc[loc])
    probBank = eval(log['probBank'].loc[loc])
    naiveBankValues = []
    probBankValues = []
    for i in range(len(naiveBank.keys())):
        naiveBankValues.append(naiveBank[str(i)])
        probBankValues.append(probBank[str(i)])
    ax = plt.gca()
    plt.plot([0,len(naiveBank.keys())],[0,0])
    pd.DataFrame(naiveBankValues).plot(title='epoch '+str(loc)+' ROI', ax=ax, label = 'naive')
    pd.DataFrame(probBankValues).plot(title='epoch '+str(loc)+' ROI', ax=ax, label = 'prob')
    plt.xlabel('matches')
    plt.ylabel('dollars')



def plot_params(path, jSize, batchsize, weightDecay):

	figsize = (10, 10)
	col = ['testPrecision', 'trainPrecision', 'trainLoss','naiveBank', 'probBank', 'kellyBank']
	try:
		mat = pd.read_csv(path, sep = '\t',)
	except:
		raise ValueError('Bad path.')
	c = '\"jSize":' + str(jSize) + ',.*?' + \
		'"batchSize":' + str(batchsize) + ',.*?' + \
		'"weightDecay":' + str(weightDecay) + ',.*?'
	c = '.*'
	cond = mat.modconfig.str.contains(c)
	if(mat[cond].empty == True):
		raise ValueError('Bad path or parameters.')
	df = mat[cond].groupby(['epoch']).mean().reset_index()
	title = "Model = " + str(mat[cond]['model'].iloc[0])

	fig, axes = plt.subplots(nrows=2, ncols=1)
	axes[0].set_title("\n".join(wrap(title)), y=1.04)
	i = 0
	for x in col:
		i += 1
		index = (i-1)//3
		fig = df[x].plot(ax=axes[index], figsize=figsize)
		if(index == 0):
			fig.legend(loc='lower right',  title='')
			fig.set_ylabel('score')
		else:
			fig.legend(loc='lower right', title='')
			fig.set_ylabel('dollars')
		fig.set_xlabel('epoch')
	plt.show()
