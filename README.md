# PROJET PLDAC #



 ** Attention ** : Bien vérifier si l'opération de création d'un TimeFrame n'influe pas sur la construction du prochain.
 (p. ex.: les données associées au prochain timeFrame sont elles mélangées?).
 
### A faire: ###

* nettoyer le code LogitModel -> MatrixFactModel.

* essayer différentes initialisations aléatoires des modèles. (mod:reset(0.1) p. ex).

* 1 csv par campagne d'expériences:
	- rendre possible plusieurs modèles dans le main.
	
** BUT **: pouvoir lancer une expériences avec:
	
	- différents paramètres du modèles
	
	- différents modèles
	
	- différents timeFrames
	
* essayer la régularisation L1?

* mettre en place le modèle temporel:

**Ouverture**: Ajouter des features additionnels dans les modèles.